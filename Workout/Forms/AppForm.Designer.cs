﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Forms
{
    partial class AppForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sideMenuPanel = new System.Windows.Forms.Panel();
            this.exerciseSelectionButton = new System.Windows.Forms.Button();
            this.trainingButton = new System.Windows.Forms.Button();
            this.progressButton = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.userMeasurementButton = new System.Windows.Forms.Button();
            this.weightButton = new System.Windows.Forms.Button();
            this.intervalButton = new System.Windows.Forms.Button();
            this.muscleButton = new System.Windows.Forms.Button();
            this.exerciseButton = new System.Windows.Forms.Button();
            this.databaseButton = new System.Windows.Forms.Button();
            this.sideMenuOpenCloseButton = new System.Windows.Forms.Button();
            this.contentParentPanel = new System.Windows.Forms.Panel();
            this.muscleGroupButton = new System.Windows.Forms.Button();
            this.sideMenuPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // sideMenuPanel
            // 
            this.sideMenuPanel.BackColor = System.Drawing.Color.DimGray;
            this.sideMenuPanel.Controls.Add(this.muscleGroupButton);
            this.sideMenuPanel.Controls.Add(this.exerciseSelectionButton);
            this.sideMenuPanel.Controls.Add(this.trainingButton);
            this.sideMenuPanel.Controls.Add(this.progressButton);
            this.sideMenuPanel.Controls.Add(this.button8);
            this.sideMenuPanel.Controls.Add(this.button7);
            this.sideMenuPanel.Controls.Add(this.userMeasurementButton);
            this.sideMenuPanel.Controls.Add(this.weightButton);
            this.sideMenuPanel.Controls.Add(this.intervalButton);
            this.sideMenuPanel.Controls.Add(this.muscleButton);
            this.sideMenuPanel.Controls.Add(this.exerciseButton);
            this.sideMenuPanel.Controls.Add(this.databaseButton);
            this.sideMenuPanel.Controls.Add(this.sideMenuOpenCloseButton);
            this.sideMenuPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.sideMenuPanel.Location = new System.Drawing.Point(0, 0);
            this.sideMenuPanel.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.sideMenuPanel.Name = "sideMenuPanel";
            this.sideMenuPanel.Size = new System.Drawing.Size(250, 1815);
            this.sideMenuPanel.TabIndex = 0;
            // 
            // exerciseSelectionButton
            // 
            this.exerciseSelectionButton.Enabled = false;
            this.exerciseSelectionButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exerciseSelectionButton.Location = new System.Drawing.Point(0, 480);
            this.exerciseSelectionButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.exerciseSelectionButton.Name = "exerciseSelectionButton";
            this.exerciseSelectionButton.Size = new System.Drawing.Size(250, 60);
            this.exerciseSelectionButton.TabIndex = 12;
            this.exerciseSelectionButton.Text = "Wochen-Übungen";
            this.exerciseSelectionButton.UseVisualStyleBackColor = true;
            this.exerciseSelectionButton.Click += new System.EventHandler(this.exerciseSelectionButton_Click);
            // 
            // trainingButton
            // 
            this.trainingButton.Enabled = false;
            this.trainingButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.trainingButton.Location = new System.Drawing.Point(0, 720);
            this.trainingButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trainingButton.Name = "trainingButton";
            this.trainingButton.Size = new System.Drawing.Size(250, 60);
            this.trainingButton.TabIndex = 11;
            this.trainingButton.Text = "Training";
            this.trainingButton.UseVisualStyleBackColor = true;
            this.trainingButton.Click += new System.EventHandler(this.trainingButton_Click);
            // 
            // progressButton
            // 
            this.progressButton.Enabled = false;
            this.progressButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.progressButton.Location = new System.Drawing.Point(0, 660);
            this.progressButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.progressButton.Name = "progressButton";
            this.progressButton.Size = new System.Drawing.Size(250, 60);
            this.progressButton.TabIndex = 10;
            this.progressButton.Text = "Fortschritt";
            this.progressButton.UseVisualStyleBackColor = true;
            this.progressButton.Click += new System.EventHandler(this.progressButton_Click);
            // 
            // button8
            // 
            this.button8.Enabled = false;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button8.Location = new System.Drawing.Point(0, 120);
            this.button8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(250, 60);
            this.button8.TabIndex = 9;
            this.button8.Text = "Stammdaten";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Enabled = false;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button7.Location = new System.Drawing.Point(0, 540);
            this.button7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(250, 60);
            this.button7.TabIndex = 8;
            this.button7.Text = "Benutzerbereich";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // userMeasurementButton
            // 
            this.userMeasurementButton.Enabled = false;
            this.userMeasurementButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.userMeasurementButton.Location = new System.Drawing.Point(0, 600);
            this.userMeasurementButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.userMeasurementButton.Name = "userMeasurementButton";
            this.userMeasurementButton.Size = new System.Drawing.Size(250, 60);
            this.userMeasurementButton.TabIndex = 7;
            this.userMeasurementButton.Text = "Körper Messungen";
            this.userMeasurementButton.UseVisualStyleBackColor = true;
            this.userMeasurementButton.Click += new System.EventHandler(this.userMeasurementButton_Click);
            // 
            // weightButton
            // 
            this.weightButton.Enabled = false;
            this.weightButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.weightButton.Location = new System.Drawing.Point(0, 360);
            this.weightButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.weightButton.Name = "weightButton";
            this.weightButton.Size = new System.Drawing.Size(250, 60);
            this.weightButton.TabIndex = 6;
            this.weightButton.Text = "Gewichte";
            this.weightButton.UseVisualStyleBackColor = true;
            this.weightButton.Click += new System.EventHandler(this.weightButton_Click);
            // 
            // intervalButton
            // 
            this.intervalButton.Enabled = false;
            this.intervalButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.intervalButton.Location = new System.Drawing.Point(0, 300);
            this.intervalButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.intervalButton.Name = "intervalButton";
            this.intervalButton.Size = new System.Drawing.Size(250, 60);
            this.intervalButton.TabIndex = 5;
            this.intervalButton.Text = "Intervalle";
            this.intervalButton.UseVisualStyleBackColor = true;
            this.intervalButton.Click += new System.EventHandler(this.intervalButton_Click);
            // 
            // muscleButton
            // 
            this.muscleButton.Enabled = false;
            this.muscleButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.muscleButton.Location = new System.Drawing.Point(0, 180);
            this.muscleButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.muscleButton.Name = "muscleButton";
            this.muscleButton.Size = new System.Drawing.Size(250, 60);
            this.muscleButton.TabIndex = 4;
            this.muscleButton.Text = "Muskeln";
            this.muscleButton.UseVisualStyleBackColor = true;
            this.muscleButton.Click += new System.EventHandler(this.muscleButton_Click);
            // 
            // exerciseButton
            // 
            this.exerciseButton.Enabled = false;
            this.exerciseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exerciseButton.Location = new System.Drawing.Point(0, 420);
            this.exerciseButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.exerciseButton.Name = "exerciseButton";
            this.exerciseButton.Size = new System.Drawing.Size(250, 60);
            this.exerciseButton.TabIndex = 3;
            this.exerciseButton.Text = "Übungen";
            this.exerciseButton.UseVisualStyleBackColor = true;
            this.exerciseButton.Click += new System.EventHandler(this.exerciseButton_Click);
            // 
            // databaseButton
            // 
            this.databaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.databaseButton.Location = new System.Drawing.Point(0, 60);
            this.databaseButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.databaseButton.Name = "databaseButton";
            this.databaseButton.Size = new System.Drawing.Size(250, 60);
            this.databaseButton.TabIndex = 2;
            this.databaseButton.Text = "Datenbank";
            this.databaseButton.UseVisualStyleBackColor = true;
            this.databaseButton.Click += new System.EventHandler(this.databaseButton_Click);
            // 
            // sideMenuOpenCloseButton
            // 
            this.sideMenuOpenCloseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sideMenuOpenCloseButton.Location = new System.Drawing.Point(0, 0);
            this.sideMenuOpenCloseButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.sideMenuOpenCloseButton.Name = "sideMenuOpenCloseButton";
            this.sideMenuOpenCloseButton.Size = new System.Drawing.Size(250, 60);
            this.sideMenuOpenCloseButton.TabIndex = 1;
            this.sideMenuOpenCloseButton.Text = " === ";
            this.sideMenuOpenCloseButton.UseVisualStyleBackColor = true;
            // 
            // contentParentPanel
            // 
            this.contentParentPanel.BackColor = System.Drawing.Color.LightGray;
            this.contentParentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contentParentPanel.Location = new System.Drawing.Point(250, 0);
            this.contentParentPanel.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.contentParentPanel.Name = "contentParentPanel";
            this.contentParentPanel.Size = new System.Drawing.Size(3297, 1815);
            this.contentParentPanel.TabIndex = 1;
            // 
            // muscleGroupButton
            // 
            this.muscleGroupButton.Enabled = false;
            this.muscleGroupButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.muscleGroupButton.Location = new System.Drawing.Point(0, 240);
            this.muscleGroupButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.muscleGroupButton.Name = "muscleGroupButton";
            this.muscleGroupButton.Size = new System.Drawing.Size(250, 60);
            this.muscleGroupButton.TabIndex = 4;
            this.muscleGroupButton.Text = "Muskelgruppen";
            this.muscleGroupButton.UseVisualStyleBackColor = true;
            this.muscleGroupButton.Click += new System.EventHandler(this.muscleGroupButton_Click);
            // 
            // AppForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(3547, 1815);
            this.Controls.Add(this.contentParentPanel);
            this.Controls.Add(this.sideMenuPanel);
            this.Location = new System.Drawing.Point(22, 22);
            this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Name = "AppForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.sideMenuPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Panel contentParentPanel;
        private System.Windows.Forms.Button databaseButton;
        private System.Windows.Forms.Button exerciseButton;
        private System.Windows.Forms.Button exerciseSelectionButton;
        private System.Windows.Forms.Button intervalButton;
        private System.Windows.Forms.Button muscleButton;
        private System.Windows.Forms.Button progressButton;
        private System.Windows.Forms.Button sideMenuOpenCloseButton;
        private System.Windows.Forms.Panel sideMenuPanel;
        private System.Windows.Forms.Button trainingButton;
        private System.Windows.Forms.Button userMeasurementButton;
        private System.Windows.Forms.Button weightButton;

        #endregion

        private Button muscleGroupButton;
    }
}