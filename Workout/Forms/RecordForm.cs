using System;
using System.Drawing;
using System.Windows.Forms;

namespace Forms
{
    public partial class RecordForm : Form
    {
        public RecordForm(Panel recordPanel)
        {
            InitializeComponent();
            
            Controls.Add(recordPanel);
            var newWidth = recordPanel.Width;
            var newHeight = Size.Height + recordPanel.Height;
            Size = new Size(newWidth, newHeight);
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            
        }
    }
}