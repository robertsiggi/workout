using System.Drawing;
using System.Windows.Forms;
using Forms.Controls;
using Microsoft.EntityFrameworkCore.Internal;
using Model;

namespace Forms.Contents
{
    public class MuscleContent: BaseContent
    {
        private readonly MuscleControl _control;
        
        private ColumnHeader _columnId;
        private ColumnHeader _columnName;
        private ColumnHeader _columnMinSetsPerWeek;
        
        private ListView _listViewMuscles;

        public MuscleContent(Panel headerPanel, Panel contentPanel, MuscleControl control) : base(headerPanel, contentPanel)
        {
            _control = control;
            InitializeContent();
        }
        
        public new void Dispose()
        {
            _columnId.Dispose();
            _columnName.Dispose();
            _columnMinSetsPerWeek.Dispose();
            _listViewMuscles.Dispose();
        }
        
        private new void InitializeContent()
        {
            _columnId = new ColumnHeader {Name = "columnId", Text = "Id", Width = 300};
            _columnName = new ColumnHeader {Name = "columnName", Text = "Muskel", Width = 400};
            _columnMinSetsPerWeek = new ColumnHeader {Name = "columnMinSetsPerWeek", Text = "Sets pro Woche", Width = 400};
            _listViewMuscles = new ListView();   
            
            _listViewMuscles.Columns.AddRange(new[]
            {
                _columnId,
                _columnName,
                _columnMinSetsPerWeek
            });
            
            _listViewMuscles.Dock = DockStyle.Fill;
            _listViewMuscles.HideSelection = false;
            _listViewMuscles.Location = new Point(150, 0);
            _listViewMuscles.Margin = new Padding(2);
            _listViewMuscles.Name = "listViewWeights";
            _listViewMuscles.Size = new Size(919, 482);
            _listViewMuscles.TabIndex = 0;
            _listViewMuscles.UseCompatibleStateImageBehavior = false;
            _listViewMuscles.View = View.Details;
            _listViewMuscles.FullRowSelect = true;
            _listViewMuscles.MultiSelect = false;
            _listViewMuscles.MouseClick += listViewMuscles_MouseClick;
            _listViewMuscles.MouseDoubleClick += listViewMuscles_MouseDoubleClick;
            
            ContentPanel.Controls.Add(_listViewMuscles);

            _control.GetMuscles(_listViewMuscles);
        }
        
        private void listViewMuscles_MouseClick(object sender, MouseEventArgs args)
        {
            var items = _listViewMuscles.SelectedItems;
            if (items == null || !items.Any())
            {
                return;
            }

            var selectedItem = items[0];
            var index = selectedItem.Index;

            if (index != 0)
            {
                return;
            }

            if (_control.NewMuscle())
            {
                _control.GetMuscles(_listViewMuscles);
            }
        }
        
        private void listViewMuscles_MouseDoubleClick(object sender, MouseEventArgs args)
        {
            var items = _listViewMuscles.SelectedItems;
            if (items == null || !items.Any())
            {
                return;
            }

            var selectedItem = items[0];
            var index = selectedItem.Index;

            if (index <= 0)
            {
                return;
            }

            var muscle = selectedItem.Tag;
            if (_control.EditMuscle(muscle as Muscle))
            {
                _control.GetMuscles(_listViewMuscles);
            }
        }
    }
}