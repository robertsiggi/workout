using System.Windows.Forms;

namespace Forms.Contents
{
    public abstract class BaseContent: IBaseContent
    {
        protected readonly Panel HeaderPanel;
        protected readonly Panel ContentPanel;

        protected BaseContent(Panel headerPanel, Panel contentPanel)
        {
            HeaderPanel = headerPanel;
            ContentPanel = contentPanel;
        }
        
        public void Dispose()
        {
        }

        protected void InitializeContent()
        {
            
        }
    }
}