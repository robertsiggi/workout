using System.Drawing;
using System.Windows.Forms;
using Forms.Controls;
using Microsoft.EntityFrameworkCore.Internal;
using Model;

namespace Forms.Contents
{
    public class UserMeasurementContent: BaseContent
    {
        private readonly UserMeasurementControl _control;
        
        private ColumnHeader _columnId;
        private ColumnHeader _columnDate;
        private ColumnHeader _columnWeight;
        private ColumnHeader _columnUpperArmLeft;
        private ColumnHeader _columnUpperArmRight;
        private ColumnHeader _columnForeArmLeft;
        private ColumnHeader _columnForeArmRight;
        private ColumnHeader _columnChest;
        private ColumnHeader _columnThighLeft;
        private ColumnHeader _columnThighRight;
        private ColumnHeader _columnCalfLeft;
        private ColumnHeader _columnCalfRight;
        private ColumnHeader _columnWaist;
        private ColumnHeader _columnShoulder;
        
        private ListView _listViewUserMeasurement;

        public UserMeasurementContent(Panel headerPanel, Panel contentPanel, UserMeasurementControl control) : base(headerPanel, contentPanel)
        {
            _control = control;
            InitializeContent();
        }
        
        public new void Dispose()
        {
            _columnId.Dispose();
            _columnDate.Dispose();
            _listViewUserMeasurement.Dispose();
        }
        
        private new void InitializeContent()
        {
            _columnId = new ColumnHeader {Name = "columnId", Text = "Id", Width = 300};
            _columnDate = new ColumnHeader {Name = "columnDate", Text = "Datum", Width = 200};
            _columnWeight = new ColumnHeader {Name = "columnWeight", Text = "Gewicht", Width = 200};
            _columnUpperArmLeft = new ColumnHeader {Name = "columnUpperArmLeft", Text = "Oberarm L", Width = 200};
            _columnUpperArmRight = new ColumnHeader {Name = "columnUpperArmRight", Text = "Oberarm R", Width = 200};
            _columnForeArmLeft = new ColumnHeader {Name = "columnForeArmLeft", Text = "Unterarm L", Width = 200};
            _columnForeArmRight = new ColumnHeader {Name = "columnForeArmRight", Text = "Unterarm R", Width = 200};
            _columnChest = new ColumnHeader {Name = "columnChest", Text = "Brust", Width = 200};
            _columnThighLeft = new ColumnHeader {Name = "columnThighLeft", Text = "Oberschenkel L", Width = 200};
            _columnThighRight = new ColumnHeader {Name = "columnThighArmRight", Text = "Oberschenkel R", Width = 200};
            _columnCalfLeft = new ColumnHeader {Name = "columnCalfArmLeft", Text = "Wade L", Width = 200};
            _columnCalfRight = new ColumnHeader {Name = "columnCalfArmRight", Text = "Wade R", Width = 200};
            _columnWaist = new ColumnHeader {Name = "columnWaist", Text = "Hüfte", Width = 200};
            _columnShoulder = new ColumnHeader {Name = "columnShoulder", Text = "Schulter", Width = 200};

            _listViewUserMeasurement = new ListView();   
            
            _listViewUserMeasurement.Columns.AddRange(new[]
            {
                _columnId,
                _columnDate,
                _columnWeight,
                _columnShoulder,
                _columnUpperArmLeft,
                _columnUpperArmRight,
                _columnForeArmLeft,
                _columnForeArmRight,
                _columnChest,
                _columnWaist,
                _columnThighLeft,
                _columnThighRight,
                _columnCalfLeft,
                _columnCalfRight
            });
            _listViewUserMeasurement.Dock = DockStyle.Fill;
            _listViewUserMeasurement.HideSelection = false;
            _listViewUserMeasurement.Location = new Point(150, 0);
            _listViewUserMeasurement.Margin = new Padding(2);
            _listViewUserMeasurement.Name = "listViewWeights";
            _listViewUserMeasurement.Size = new Size(919, 482);
            _listViewUserMeasurement.TabIndex = 0;
            _listViewUserMeasurement.UseCompatibleStateImageBehavior = false;
            _listViewUserMeasurement.View = View.Details;
            _listViewUserMeasurement.FullRowSelect = true;
            _listViewUserMeasurement.MultiSelect = false;
            _listViewUserMeasurement.MouseClick += listViewUserMeasurement_MouseClick;
            _listViewUserMeasurement.MouseDoubleClick += listViewUserMeasurement_MouseDoubleClick;
            
            ContentPanel.Controls.Add(_listViewUserMeasurement);
            
            _control.GetUserMeasurements(_listViewUserMeasurement);
        }

        private void listViewUserMeasurement_MouseClick(object sender, MouseEventArgs args)
        {
            var items = _listViewUserMeasurement.SelectedItems;
            if (items == null || !items.Any())
            {
                return;
            }

            var selectedItem = items[0];
            var index = selectedItem.Index;

            if (index != 0)
            {
                return;
            }

            if (_control.NewUserMeasurement())
            {
                _control.GetUserMeasurements(_listViewUserMeasurement);
            }
        }
        
        private void listViewUserMeasurement_MouseDoubleClick(object sender, MouseEventArgs args)
        {
            var items = _listViewUserMeasurement.SelectedItems;
            if (items == null || !items.Any())
            {
                return;
            }

            var selectedItem = items[0];
            var index = selectedItem.Index;

            if (index <= 0)
            {
                return;
            }

            var userMeasurement = selectedItem.Tag;
            if (_control.EditUserMeasurement(userMeasurement as UserMeasurement))
            {
                _control.GetUserMeasurements(_listViewUserMeasurement);
            }
        }
    }
}