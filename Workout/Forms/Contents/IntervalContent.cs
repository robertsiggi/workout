using System;
using System.Drawing;
using System.Windows.Forms;
using Database;
using Forms.Controls;
using Microsoft.EntityFrameworkCore.Internal;
using Model;

namespace Forms.Contents
{
    public class IntervalContent: BaseContent
    {
        private readonly IntervalControl _control;
        
        private ColumnHeader _columnId;
        private ColumnHeader _columnSets;
        private ColumnHeader _columnRepsMin;
        private ColumnHeader _columnRepsMax;
        private ColumnHeader _columnRepDuration;
        private ColumnHeader _columnPause;
        
        private ListView _listViewIntervals;

        public IntervalContent(Panel headerPanel, Panel contentPanel, IntervalControl control) : base(headerPanel, contentPanel)
        {
            _control = control;
            InitializeContent();
        }
        
        public new void Dispose()
        {
            _columnId.Dispose();
            _columnSets.Dispose();
            _columnRepsMin.Dispose();
            _columnRepsMax.Dispose();
            _columnRepDuration.Dispose();
            _columnPause.Dispose();
            _listViewIntervals.Dispose();
        }
        
        private new void InitializeContent()
        {
            _columnId = new ColumnHeader {Name = "columnId", Text = "Id", Width = 300};
            _columnSets = new ColumnHeader {Name = "columnSets", Text = "Sets", Width = 400};
            _columnRepsMin = new ColumnHeader {Name = "columnRepsMin", Text = "min. Reps", Width = 400};
            _columnRepsMax = new ColumnHeader {Name = "columnRepsMax", Text = "max. Reps", Width = 400};
            _columnRepDuration = new ColumnHeader {Name = "columnRepsDuration", Text = "Rep Dauer", Width = 400};
            _columnPause = new ColumnHeader {Name = "columnPause", Text = "Pause", Width = 400};
            
            _listViewIntervals = new ListView();   
            
            _listViewIntervals.Columns.AddRange(new[]
            {
                _columnId,
                _columnSets,
                _columnRepsMin,
                _columnRepsMax,
                _columnRepDuration,
                _columnPause
            });
            
            _listViewIntervals.Dock = DockStyle.Fill;
            _listViewIntervals.HideSelection = false;
            _listViewIntervals.Location = new Point(150, 0);
            _listViewIntervals.Margin = new Padding(2);
            _listViewIntervals.Name = "listViewWeights";
            _listViewIntervals.Size = new Size(919, 482);
            _listViewIntervals.TabIndex = 0;
            _listViewIntervals.UseCompatibleStateImageBehavior = false;
            _listViewIntervals.View = View.Details;
            _listViewIntervals.MultiSelect = false;
            _listViewIntervals.MouseClick += listViewIntervals_MouseClick;
            _listViewIntervals.MouseDoubleClick += listViewIntervals_MouseDoubleClick;
            _listViewIntervals.FullRowSelect = true;
            
            ContentPanel.Controls.Add(_listViewIntervals);

            _control.GetIntervals(_listViewIntervals);
        }
        
        private void listViewIntervals_MouseClick(object sender, MouseEventArgs args)
        {
            var items = _listViewIntervals.SelectedItems;
            if (items == null || !items.Any())
            {
                return;
            }

            var selectedItem = items[0];
            var index = selectedItem.Index;

            if (index != 0)
            {
                return;
            }

            if (_control.NewInterval())
            {
                _control.GetIntervals(_listViewIntervals);
            }
        }
        
        private void listViewIntervals_MouseDoubleClick(object sender, MouseEventArgs args)
        {
            var items = _listViewIntervals.SelectedItems;
            if (items == null || !items.Any())
            {
                return;
            }

            var selectedItem = items[0];
            var index = selectedItem.Index;

            if (index <= 0)
            {
                return;
            }

            var interval = selectedItem.Tag;
            if (_control.EditInterval(interval as Interval))
            {
                _control.GetIntervals(_listViewIntervals);
            }
        }
    }
}