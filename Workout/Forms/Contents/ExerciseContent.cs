using System;
using System.Drawing;
using System.Windows.Forms;
using Database;
using Forms.Controls;
using Microsoft.EntityFrameworkCore.Internal;
using Model;

namespace Forms.Contents
{
    public class ExerciseContent: BaseContent
    {
        private readonly ExerciseControl _control;
        
        private ColumnHeader _columnId;
        private ColumnHeader _columnName;
        private ColumnHeader _columnDescription;
        private ColumnHeader _columnIsBodyWeight;
        private ColumnHeader _columnIsCompoundExercise;
        private ColumnHeader _columnMuscle1;
        private ColumnHeader _columnMuscle2;
        private ColumnHeader _externalRef1;
        private ColumnHeader _externalRef2;
        
        private ListView _listViewExercises;

        public ExerciseContent(Panel headerPanel, Panel contentPanel, ExerciseControl control) : base(headerPanel, contentPanel)
        {
            _control = control;
            InitializeContent();
        }
        
        public new void Dispose()
        {
            _columnId.Dispose();
            _columnName.Dispose();
            _columnDescription.Dispose();
            _columnIsBodyWeight.Dispose();
            _columnIsCompoundExercise.Dispose();
            _columnMuscle1.Dispose();
            _columnMuscle2.Dispose();
            _externalRef1.Dispose();
            _externalRef2.Dispose();
            _listViewExercises.Dispose();
        }
        
        private new void InitializeContent()
        {
            _columnId = new ColumnHeader {Name = "columnId", Text = "Id", Width = 300};
            _columnName = new ColumnHeader {Name = "columnName", Text = "Übung", Width = 400};
            _columnDescription = new ColumnHeader {Name = "columnDescriptiom", Text = "Beschreibung", Width = 400};
            _columnIsBodyWeight = new ColumnHeader {Name = "columnIsBodyWeight", Text = "Körpergewichtsübung", Width = 300};
            _columnIsCompoundExercise = new ColumnHeader {Name = "columnIsCompoundExercise", Text = "Verbundübung", Width = 300};
            _columnMuscle1 = new ColumnHeader {Name = "columnMuscle1", Text = "Muskel 1", Width = 300};
            _columnMuscle2 = new ColumnHeader {Name = "columnMuscle2", Text = "Muskel 2", Width = 300};
            _externalRef1 = new ColumnHeader {Name = "columnExternalRef1", Text = "Verweis 1", Width = 300};
            _externalRef2 = new ColumnHeader {Name = "columnExternalRef2", Text = "Verweis 2", Width = 300};
            _listViewExercises = new ListView();   
            
            _listViewExercises.Columns.AddRange(new[]
            {
                _columnId,
                _columnName,
                _columnDescription,
                _columnIsBodyWeight,
                _columnIsCompoundExercise,
                _columnMuscle1,
                _columnMuscle2,
                _externalRef1,
                _externalRef2
            });
            
            _listViewExercises.Dock = DockStyle.Fill;
            _listViewExercises.HideSelection = false;
            _listViewExercises.Location = new Point(150, 0);
            _listViewExercises.Margin = new Padding(2);
            _listViewExercises.Name = "listViewExercises";
            _listViewExercises.Size = new Size(919, 482);
            _listViewExercises.TabIndex = 0;
            _listViewExercises.UseCompatibleStateImageBehavior = false;
            _listViewExercises.View = View.Details;
            _listViewExercises.FullRowSelect = true;
            _listViewExercises.MouseClick += _listViewExercises_MouseClick;
            _listViewExercises.MouseDoubleClick += _listViewExercises_MouseDoubleClick;
            
            
            ContentPanel.Controls.Add(_listViewExercises);

            _control.GetExercises(_listViewExercises);
        }
        
        private void _listViewExercises_MouseClick(object sender, MouseEventArgs args)
        {
            var items = _listViewExercises.SelectedItems;
            if (items == null || !items.Any())
            {
                return;
            }

            var selectedItem = items[0];
            var index = selectedItem.Index;

            if (index != 0)
            {
                return;
            }

            if (_control.NewExercise())
            {
                _control.GetExercises(_listViewExercises);
            }
        }
        
        private void _listViewExercises_MouseDoubleClick(object sender, MouseEventArgs args)
        {
            var items = _listViewExercises.SelectedItems;
            if (items == null || !items.Any())
            {
                return;
            }

            var selectedItem = items[0];
            var index = selectedItem.Index;

            if (index <= 0)
            {
                return;
            }

            var exercise = selectedItem.Tag;
            if (_control.EditExercise(exercise as Exercise))
            {
                _control.GetExercises(_listViewExercises);
            }
        }
    }
}