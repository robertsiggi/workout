using System;
using System.Drawing;
using System.Windows.Forms;
using Database;
using Forms.Controls;

namespace Forms.Contents
{
    public class DatabaseContent: BaseContent, IBaseContent
    {
        private readonly DatabaseControl _control;
        private Button _buttonLoadDatabase;
        private Button _buttonSelectDatabase;
        private TextBox _textBoxDatabaseFilePath;
        
        public DatabaseContent(Panel headerPanel, Panel contentPanel, DatabaseControl control) : base(headerPanel, contentPanel)
        {
            _control = control;
            InitializeContent();
        }

        public new void Dispose()
        {
            _buttonLoadDatabase.Dispose();
            _buttonSelectDatabase.Dispose();
            _textBoxDatabaseFilePath.Dispose();
        }
        
        private new void InitializeContent()
        {
            _textBoxDatabaseFilePath = new TextBox
            {
                Location = new Point(10, 10),
                Margin = new Padding(2),
                Name = "textBoxDatabaseFilePath",
                Size = new Size(1200, 20),
                TabIndex = 1,
                Text = "C:\\dev\\Projects\\Workout\\Workout\\Console\\bin\\Debug\\netcoreapp3.1\\workout.db"
            };

            _buttonSelectDatabase = new Button
            {
                Location = new Point(10, 50),
                Margin = new Padding(2),
                Name = "buttonSelectDatabase",
                Size = new Size(200, 35),
                TabIndex = 2,
                Text = "Datenbank auswählen",
                UseVisualStyleBackColor = true
            };
            _buttonSelectDatabase.Click += ButtonSelectDatabase_Click;

            _buttonLoadDatabase = new Button
            {
                Location = new Point(210, 50),
                Margin = new Padding(2),
                Name = "buttonLoadDatabase",
                Size = new Size(200, 35),
                TabIndex = 3,
                Text = "Datenbank laden",
                UseVisualStyleBackColor = true
            };
            _buttonLoadDatabase.Click += ButtonLoadDatabase_Click;

            if (_control.HasDatabaseLoaded)
            {
                _buttonSelectDatabase.Enabled = false;
                _buttonLoadDatabase.Enabled = false;
                _textBoxDatabaseFilePath.Enabled = false;
                _textBoxDatabaseFilePath.Text = DatabaseContextFactory.ConnectionString;
            }
            
            ContentPanel.Controls.Add(_textBoxDatabaseFilePath);
            ContentPanel.Controls.Add(_buttonSelectDatabase);
            ContentPanel.Controls.Add(_buttonLoadDatabase);
        }

        private void ButtonLoadDatabase_Click(object sender, EventArgs e)
        {
            _control.LoadDatabase(_textBoxDatabaseFilePath.Text);
            _buttonSelectDatabase.Enabled = false;
            _buttonLoadDatabase.Enabled = false;
            _textBoxDatabaseFilePath.Enabled = false;
        }

        private void ButtonSelectDatabase_Click(object sender, EventArgs e)
        {
            var databaseFilePath = _control.SelectDatabase();
            if (string.IsNullOrEmpty(databaseFilePath))
            {
                return;
            }

            _textBoxDatabaseFilePath.Text = databaseFilePath;
        }
    }
}