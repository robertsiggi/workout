using System;
using System.Drawing;
using System.Windows.Forms;
using Database;
using Forms.Controls;
using Microsoft.EntityFrameworkCore.Internal;
using Model;

namespace Forms.Contents
{
    public class ProgressContent: BaseContent
    {
        private readonly ProgressControl _control;
        
        private ColumnHeader _columnId;
        private ColumnHeader _columnExercise;
        private ColumnHeader _columnWeight;
        private ColumnHeader _columnSets;
        private ColumnHeader _columnRepsMin;
        private ColumnHeader _columnRepsMax;
        private ColumnHeader _columnRepDuration;
        private ColumnHeader _columnPause;
        private ListView _listViewProgresses;

        public ProgressContent(Panel headerPanel, Panel contentPanel, ProgressControl control) : base(headerPanel, contentPanel)
        {
            _control = control;
            InitializeContent();
        }
        
        public new void Dispose()
        {
            _columnId.Dispose();
            _columnExercise.Dispose();
            _columnWeight.Dispose();
            _columnSets.Dispose();
            _columnRepsMin.Dispose();
            _columnRepsMax.Dispose();
            _columnRepDuration.Dispose();
            _columnPause.Dispose();
            _listViewProgresses.Dispose();
        }
        
        private new void InitializeContent()
        {
            _columnId = new ColumnHeader {Name = "columnId", Text = "Id", Width = 300};
            _columnExercise = new ColumnHeader {Name = "column", Text = "Übung", Width = 400};
            _columnWeight = new ColumnHeader {Name = "columnWeight", Text = "Gewicht", Width = 400};
            _columnSets = new ColumnHeader {Name = "columnSets", Text = "Sets", Width = 400};
            _columnRepsMin = new ColumnHeader {Name = "columnRepsMin", Text = "min. Reps", Width = 400};
            _columnRepsMax = new ColumnHeader {Name = "columnRepsMax", Text = "max. Reps", Width = 400};
            _columnRepDuration = new ColumnHeader {Name = "columnRepDuration", Text = "Rep Dauer", Width = 400};
            _columnPause = new ColumnHeader {Name = "columnPause", Text = "Pause", Width = 400};
            _listViewProgresses = new ListView();   
            
            _listViewProgresses.Columns.AddRange(new[]
            {
                _columnId,
                _columnExercise,
                _columnSets,
                _columnWeight,
                _columnRepsMin,
                _columnRepsMax,
                _columnRepDuration,
                _columnPause
            });
            
            _listViewProgresses.Dock = DockStyle.Fill;
            _listViewProgresses.HideSelection = false;
            _listViewProgresses.Location = new Point(150, 0);
            _listViewProgresses.Margin = new Padding(2);
            _listViewProgresses.Name = "listViewProgresss";
            _listViewProgresses.Size = new Size(919, 482);
            _listViewProgresses.TabIndex = 0;
            _listViewProgresses.UseCompatibleStateImageBehavior = false;
            _listViewProgresses.View = View.Details;
            _listViewProgresses.FullRowSelect = true;
            _listViewProgresses.MouseClick += ListViewProgressesMouseClick;
            _listViewProgresses.MouseDoubleClick += ListViewProgressesMouseDoubleClick;
            
            ContentPanel.Controls.Add(_listViewProgresses);

            _control.GetProgresss(_listViewProgresses);
        }
        
        private void ListViewProgressesMouseClick(object sender, MouseEventArgs args)
        {
            var items = _listViewProgresses.SelectedItems;
            if (items == null || !items.Any())
            {
                return;
            }

            var selectedItem = items[0];
            var index = selectedItem.Index;

            if (index != 0)
            {
                return;
            }

            if (_control.NewProgress())
            {
                _control.GetProgresss(_listViewProgresses);
            }
        }
        
        private void ListViewProgressesMouseDoubleClick(object sender, MouseEventArgs args)
        {
            var items = _listViewProgresses.SelectedItems;
            if (items == null || !items.Any())
            {
                return;
            }

            var selectedItem = items[0];
            var index = selectedItem.Index;

            if (index <= 0)
            {
                return;
            }

            var progress = selectedItem.Tag;
            if (_control.EditProgress(progress as Progress))
            {
                _control.GetProgresss(_listViewProgresses);
            }
        }
    }
}