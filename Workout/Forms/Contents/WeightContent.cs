using System;
using System.Drawing;
using System.Windows.Forms;
using Database;
using Forms.Controls;
using Microsoft.EntityFrameworkCore.Internal;
using Model;

namespace Forms.Contents
{
    public class WeightContent: BaseContent
    {
        private readonly WeightControl _control;
        
        private ColumnHeader _columnId;
        private ColumnHeader _columnWeight;
        
        private ListView _listViewWeights;

        public WeightContent(Panel headerPanel, Panel contentPanel, WeightControl control) : base(headerPanel, contentPanel)
        {
            _control = control;
            InitializeContent();
        }
        
        public new void Dispose()
        {
            _columnId.Dispose();
            _columnWeight.Dispose();
            _listViewWeights.Dispose();
        }
        
        private new void InitializeContent()
        {
            _columnId = new ColumnHeader {Name = "columnId", Text = "Id", Width = 300};
            _columnWeight = new ColumnHeader {Name = "columnWeight", Text = "Gewicht", Width = 400};
            _listViewWeights = new ListView();   
            
            _listViewWeights.Columns.AddRange(new[]
            {
                _columnId,
                _columnWeight
            });
            
            _listViewWeights.Dock = DockStyle.Fill;
            _listViewWeights.HideSelection = false;
            _listViewWeights.Location = new Point(150, 0);
            _listViewWeights.Margin = new Padding(2);
            _listViewWeights.Name = "listViewWeights";
            _listViewWeights.Size = new Size(919, 482);
            _listViewWeights.TabIndex = 0;
            _listViewWeights.UseCompatibleStateImageBehavior = false;
            _listViewWeights.View = View.Details;
            _listViewWeights.FullRowSelect = true;
            _listViewWeights.MouseClick += listViewWeights_MouseClick;
            _listViewWeights.MouseDoubleClick += listViewWeights_MouseDoubleClick;
            
            ContentPanel.Controls.Add(_listViewWeights);

            _control.GetWeights(_listViewWeights);
        }
        
        private void listViewWeights_MouseClick(object sender, MouseEventArgs args)
        {
            var items = _listViewWeights.SelectedItems;
            if (items == null || !items.Any())
            {
                return;
            }

            var selectedItem = items[0];
            var index = selectedItem.Index;

            if (index != 0)
            {
                return;
            }

            if (_control.NewWeight())
            {
                _control.GetWeights(_listViewWeights);
            }
        }
        
        private void listViewWeights_MouseDoubleClick(object sender, MouseEventArgs args)
        {
            var items = _listViewWeights.SelectedItems;
            if (items == null || !items.Any())
            {
                return;
            }

            var selectedItem = items[0];
            var index = selectedItem.Index;

            if (index <= 0)
            {
                return;
            }

            var weight = selectedItem.Tag;
            if (_control.EditWeight(weight as Weight))
            {
                _control.GetWeights(_listViewWeights);
            }
        }
    }
}