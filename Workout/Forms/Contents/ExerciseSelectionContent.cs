using System;
using System.Drawing;
using System.Windows.Forms;
using Database;
using Forms.Controls;
using Microsoft.EntityFrameworkCore.Internal;
using Model;

namespace Forms.Contents
{
    public class ExerciseSelectionContent: BaseContent
    {
        private readonly ExerciseSelectionControl _control;
        
        private ColumnHeader _columnId;
        private ColumnHeader _columnName;
        private ColumnHeader _columnSetsPerWeek;
        private ColumnHeader _columnExercise1;
        private ColumnHeader _columnExercise2;
        private ColumnHeader _columnExercise3;
        private ColumnHeader _columnExercise4;
        
        private ListView _listViewExerciseSelections;

        public ExerciseSelectionContent(Panel headerPanel, Panel contentPanel, ExerciseSelectionControl control) : base(headerPanel, contentPanel)
        {
            _control = control;
            InitializeContent();
        }
        
        public new void Dispose()
        {
            _columnId.Dispose();
            _columnName.Dispose();
            _columnSetsPerWeek.Dispose();
            _columnExercise1.Dispose();
            _columnExercise2.Dispose();
            _columnExercise3.Dispose();
            _columnExercise4.Dispose();
            _listViewExerciseSelections.Dispose();
        }
        
        private new void InitializeContent()
        {
            _columnId = new ColumnHeader {Name = "columnId", Text = "Id", Width = 300};
            _columnName = new ColumnHeader {Name = "columnName", Text = "Auswahl", Width = 400};
            _columnSetsPerWeek = new ColumnHeader {Name = "columnSetsPerWeek", Text = "Sets pro Woche", Width = 400};
            _columnExercise1 = new ColumnHeader {Name = "columnExercise1", Text = "Übung 1", Width = 400};
            _columnExercise2 = new ColumnHeader {Name = "columnExercise2", Text = "Übung 2", Width = 400};
            _columnExercise3 = new ColumnHeader {Name = "columnExercise3", Text = "Übung 3", Width = 400};
            _columnExercise4 = new ColumnHeader {Name = "columnExercise4", Text = "Übung 4", Width = 400};
            _listViewExerciseSelections = new ListView();   
            
            _listViewExerciseSelections.Columns.AddRange(new[]
            {
                _columnId,
                _columnName,
                _columnSetsPerWeek,
                _columnExercise1,
                _columnExercise2,
                _columnExercise3,
                _columnExercise4
            });
            
            _listViewExerciseSelections.Dock = DockStyle.Fill;
            _listViewExerciseSelections.HideSelection = false;
            _listViewExerciseSelections.Location = new Point(150, 0);
            _listViewExerciseSelections.Margin = new Padding(2);
            _listViewExerciseSelections.Name = "listViewExerciseSelections";
            _listViewExerciseSelections.Size = new Size(919, 482);
            _listViewExerciseSelections.TabIndex = 0;
            _listViewExerciseSelections.UseCompatibleStateImageBehavior = false;
            _listViewExerciseSelections.View = View.Details;
            _listViewExerciseSelections.FullRowSelect = true;
            _listViewExerciseSelections.MouseClick += listViewExerciseSelections_MouseClick;
            _listViewExerciseSelections.MouseDoubleClick += listViewExerciseSelections_MouseDoubleClick;
            
            ContentPanel.Controls.Add(_listViewExerciseSelections);

            _control.GetExerciseSelections(_listViewExerciseSelections);
        }
        
        private void listViewExerciseSelections_MouseClick(object sender, MouseEventArgs args)
        {
            var items = _listViewExerciseSelections.SelectedItems;
            if (items == null || !items.Any())
            {
                return;
            }

            var selectedItem = items[0];
            var index = selectedItem.Index;

            if (index != 0)
            {
                return;
            }

            if (_control.NewExerciseSelection())
            {
                _control.GetExerciseSelections(_listViewExerciseSelections);
            }
        }
        
        private void listViewExerciseSelections_MouseDoubleClick(object sender, MouseEventArgs args)
        {
            var items = _listViewExerciseSelections.SelectedItems;
            if (items == null || !items.Any())
            {
                return;
            }

            var selectedItem = items[0];
            var index = selectedItem.Index;

            if (index <= 0)
            {
                return;
            }

            var exerciseSelection = selectedItem.Tag;
            if (_control.EditExerciseSelection(exerciseSelection as ExerciseSelection))
            {
                _control.GetExerciseSelections(_listViewExerciseSelections);
            }
        }
    }
}