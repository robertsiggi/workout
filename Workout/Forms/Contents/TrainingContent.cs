using System;
using System.Drawing;
using System.Windows.Forms;
using Database;
using Forms.Controls;
using Microsoft.EntityFrameworkCore.Internal;
using Model;

namespace Forms.Contents
{
    public class TrainingContent: BaseContent
    {
        private readonly TrainingControl _control;
        
        private ColumnHeader _columnId;
        private ColumnHeader _columnName;
        private ColumnHeader _columnMinSetsPerWeek;
        
        private ListView _listViewTrainings;

        public TrainingContent(Panel headerPanel, Panel contentPanel, TrainingControl control) : base(headerPanel, contentPanel)
        {
            _control = control;
            InitializeContent();
        }
        
        public new void Dispose()
        {
            _columnId.Dispose();
            _columnName.Dispose();
            _columnMinSetsPerWeek.Dispose();
            _listViewTrainings.Dispose();
        }
        
        private new void InitializeContent()
        {
            _columnId = new ColumnHeader {Name = "columnId", Text = "Id", Width = 300};
            _columnName = new ColumnHeader {Name = "columnName", Text = "Muskel", Width = 400};
            _columnMinSetsPerWeek = new ColumnHeader {Name = "columnMinSetsPerWeek", Text = "Sets pro Woche", Width = 400};
            _listViewTrainings = new ListView();   
            
            _listViewTrainings.Columns.AddRange(new[]
            {
                _columnId,
                _columnName,
                _columnMinSetsPerWeek
            });
            
            _listViewTrainings.Dock = DockStyle.Fill;
            _listViewTrainings.HideSelection = false;
            _listViewTrainings.Location = new Point(150, 0);
            _listViewTrainings.Margin = new Padding(2);
            _listViewTrainings.Name = "listViewWeights";
            _listViewTrainings.Size = new Size(919, 482);
            _listViewTrainings.TabIndex = 0;
            _listViewTrainings.UseCompatibleStateImageBehavior = false;
            _listViewTrainings.View = View.Details;
            _listViewTrainings.FullRowSelect = true;
            _listViewTrainings.MultiSelect = false;
            _listViewTrainings.MouseClick += listViewTrainings_MouseClick;
            _listViewTrainings.MouseDoubleClick += listViewTrainings_MouseDoubleClick;
            
            ContentPanel.Controls.Add(_listViewTrainings);

            _control.GetTrainings(_listViewTrainings);
        }
        
        private void listViewTrainings_MouseClick(object sender, MouseEventArgs args)
        {
            var items = _listViewTrainings.SelectedItems;
            if (items == null || !items.Any())
            {
                return;
            }

            var selectedItem = items[0];
            var index = selectedItem.Index;

            if (index != 0)
            {
                return;
            }

            if (_control.NewTraining())
            {
                _control.GetTrainings(_listViewTrainings);
            }
        }
        
        private void listViewTrainings_MouseDoubleClick(object sender, MouseEventArgs args)
        {
            var items = _listViewTrainings.SelectedItems;
            if (items == null || !items.Any())
            {
                return;
            }

            var selectedItem = items[0];
            var index = selectedItem.Index;

            if (index <= 0)
            {
                return;
            }

            var training = selectedItem.Tag;
            if (_control.EditTraining(training as Training))
            {
                _control.GetTrainings(_listViewTrainings);
            }
        }
    }
}