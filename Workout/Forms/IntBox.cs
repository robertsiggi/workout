using System.Windows.Forms;

namespace Forms
{
    public class IntBox: TextBox
    {
        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) && (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
            
            base.OnKeyPress(e);
        }
    }
}