﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Forms.Contents;
using Forms.Controls;

namespace Forms
{
    public partial class AppForm : Form
    {
        private Panel _currentContentPanel;
        private Panel _currentHeaderPanel;
        private IBaseControl _currentControl;
        private IBaseContent _currentContent;
        
        public AppForm()
        {
            InitializeComponent();
        }

        private void userMeasurementButton_Click(object sender, EventArgs e)
        {
            Text = ((Button) sender).Text;
            
            ClearContent();
            
            var control = new UserMeasurementControl();
            _currentControl = control;
            _currentContent = new UserMeasurementContent(_currentHeaderPanel, _currentContentPanel, control);
        }

        private void databaseButton_Click(object sender, EventArgs e)
        {
            Text = ((Button) sender).Text;
            
            ClearContent();
            
            var control = new DatabaseControl();
            control.AfterDatabaseLoadingEvent(ActivateButtons);
            _currentControl = control;
            _currentContent = new DatabaseContent(_currentHeaderPanel, _currentContentPanel, control);
        }
        
        private void weightButton_Click(object sender, EventArgs e)
        {
            Text = ((Button) sender).Text;
            
            ClearContent();
            
            var control = new WeightControl();
            _currentControl = control;
            _currentContent = new WeightContent(_currentHeaderPanel, _currentContentPanel, control);
        }

        private void intervalButton_Click(object sender, EventArgs e)
        {
            Text = ((Button)sender).Text;

            ClearContent();

            var control = new IntervalControl();
            _currentControl = control;
            _currentContent = new IntervalContent(_currentHeaderPanel, _currentContentPanel, control);
        }

        private void muscleGroupButton_Click(object sender, EventArgs e)
        {
            Text = ((Button)sender).Text;

            ClearContent();

            var control = new IntervalControl();
            _currentControl = control;
            _currentContent = new IntervalContent(_currentHeaderPanel, _currentContentPanel, control);
        }

        private void muscleButton_Click(object sender, EventArgs e)
        {
            Text = ((Button) sender).Text;
            
            ClearContent();
            
            var control = new MuscleControl();
            _currentControl = control;
            _currentContent = new MuscleContent(_currentHeaderPanel, _currentContentPanel, control);
        }
        
        private void exerciseButton_Click(object sender, EventArgs e)
        {
            Text = ((Button) sender).Text;
            
            ClearContent();
            
            var control = new ExerciseControl();
            _currentControl = control;
            _currentContent = new ExerciseContent(_currentHeaderPanel, _currentContentPanel, control);
        }
        
        private void exerciseSelectionButton_Click(object sender, EventArgs e)
        {
            Text = ((Button) sender).Text;
            
            ClearContent();
            
            var control = new ExerciseSelectionControl();
            _currentControl = control;
            _currentContent = new ExerciseSelectionContent(_currentHeaderPanel, _currentContentPanel, control);
        }
        
        private void progressButton_Click(object sender, EventArgs e)
        {
            Text = ((Button) sender).Text;
            
            ClearContent();

            var control = new ProgressControl();
            _currentControl = control;
            _currentContent = new ProgressContent(_currentHeaderPanel, _currentContentPanel, control);
        }
        
        private void trainingButton_Click(object sender, EventArgs e)
        {
            Text = ((Button) sender).Text;
            
            ClearContent();

            var control = new TrainingControl();
            _currentControl = control;
            _currentContent = new TrainingContent(_currentHeaderPanel, _currentContentPanel, control);
        }

        private void ClearContent()
        {
            CloseCurrentComponent();
            AddContentPanel();
        }
        
        private void CloseCurrentComponent()
        {
            _currentControl?.Dispose();
            _currentContent?.Dispose();
            _currentHeaderPanel?.Dispose();
            _currentContentPanel?.Dispose();
        }
        
        private void AddContentPanel()
        {
            _currentHeaderPanel = new Panel
            {
                BackColor = Color.DimGray,
                Dock = DockStyle.Top,
                Location = new Point(150, 0),
                Name = "contentHeaderPanel",
                Size = new Size(1978, 100),
                TabIndex = 2
            };
            
            _currentContentPanel = new Panel
            {
                BackColor = Color.LightGray,
                Dock = DockStyle.Fill,
                Location = new Point(150, 0),
                Name = "contentPanel",
                Size = new Size(1978, 944),
                TabIndex = 1
            };
            
            //contentParentPanel.Controls.Add(_currentHeaderPanel);
            contentParentPanel.Controls.Add(_currentContentPanel);
        }

        private void ActivateButtons()
        {
            exerciseButton.Enabled = true;
            intervalButton.Enabled = true;
            weightButton.Enabled = true;
            muscleButton.Enabled = true;
            exerciseSelectionButton.Enabled = true;
            userMeasurementButton.Enabled = true;
            progressButton.Enabled = true;
            trainingButton.Enabled = true;
        }
    }
}