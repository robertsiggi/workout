using Database;

namespace Forms.Controls
{
    public abstract class BaseControl: IBaseControl
    {
        public bool HasDatabaseLoaded => DatabaseContext != null;
        
        protected static IDatabaseContext DatabaseContext;
        
        public void Dispose()
        {
            
        }
    }
}