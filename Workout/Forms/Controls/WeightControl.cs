using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Model;

namespace Forms.Controls
{
    public class WeightControl: BaseControl, IBaseControl
    {
        private TextBox _weightInput;
                
        public void GetWeights(ListView listView)
        {
            listView.Items.Clear();

            var weights = DatabaseContext.Weights.ToList();


            listView.Items.Add(new ListViewItem("*"));
            
            foreach (var weight in weights)
            {
                var item = new ListViewItem(weight.Id.ToString());
                item.SubItems.Add(weight.Name);

                item.Tag = weight;

                listView.Items.Add(item);
            }
        }
        
        public bool NewWeight()
        {
            var weight = new Weight();
            var panel = CreateRecordPanel(weight);
            
            var recordForm = new RecordForm(panel);
            var dialogResult = recordForm.ShowDialog();
            if (dialogResult != DialogResult.OK)
            {
                return false;
            }

            PanelToObject(weight);

            DatabaseContext.Weights.Add(weight);
            DatabaseContext.SaveChanges();

            return true;
        }

        public bool EditWeight(Weight weight)
        {
            var panel = CreateRecordPanel(weight);
            
            var recordForm = new RecordForm(panel);
            var dialogResult = recordForm.ShowDialog();
            if (dialogResult != DialogResult.OK)
            {
                return false;
            }

            PanelToObject(weight);

            DatabaseContext.Weights.Update(weight);
            DatabaseContext.SaveChanges();

            return true;
        }

        private void PanelToObject(Weight weight)
        {
            weight.Name = _weightInput.Text;
        }

        private Panel CreateRecordPanel(Weight weight)
        {
            var panel = new Panel
            {
                BackColor = Color.LightGray,
                Location = new Point(0, 0),
                Name = "recordPanel",
                Size = new Size(1200, 380),
                TabIndex = 1
            };
            
            
            var weightLabel = new Label
            {
                Text = "Gewicht",
                Location = new Point(10, 10),
                Size = new Size(200, 30)
            };
            _weightInput = new TextBox
            {
                Text = weight.Name,
                Location = new Point(220, 10),
                Size = new Size(600, 30),
                TabIndex = 3
            };
            
            panel.Controls.Add(weightLabel);
            panel.Controls.Add(_weightInput);

            return panel;
        }
    }
}