using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Model;

namespace Forms.Controls
{
    public class IntervalControl: BaseControl, IBaseControl
    {
        private IntBox _setsInput;
        private IntBox _repsMinInput;
        private IntBox _repsMaxInput;
        private IntBox _repDurationInput;
        private TextBox _pauseInput;
        
        public void GetIntervals(ListView listView)
        {
            listView.Items.Clear();
            
            var intervals = DatabaseContext.Intervals.ToList();
            
            listView.Items.Add(new ListViewItem("*"));
            
            foreach (var interval in intervals)
            {
                var item = new ListViewItem(interval.Id.ToString());
                item.SubItems.Add(interval.Sets.ToString());
                item.SubItems.Add(interval.RepsMin.ToString());
                item.SubItems.Add(interval.RepsMax.ToString());
                item.SubItems.Add(interval.RepDuration.ToString());
                item.SubItems.Add(interval.Pause ?? "");

                item.Tag = interval;

                listView.Items.Add(item);
            }
        }
        
        public bool NewInterval()
        {
            var interval = new Interval();
            var panel = CreateRecordPanel(interval);
            
            var recordForm = new RecordForm(panel);
            var dialogResult = recordForm.ShowDialog();
            if (dialogResult != DialogResult.OK)
            {
                return false;
            }

            PanelToObject(interval);

            DatabaseContext.Intervals.Add(interval);
            DatabaseContext.SaveChanges();


            return true;
        }

        public bool EditInterval(Interval interval)
        {
            var panel = CreateRecordPanel(interval);
            
            var recordForm = new RecordForm(panel);
            var dialogResult = recordForm.ShowDialog();
            if (dialogResult != DialogResult.OK)
            {
                return false;
            }

            PanelToObject(interval);

            DatabaseContext.Intervals.Update(interval);
            DatabaseContext.SaveChanges();

            return true;
        }

        private void PanelToObject(Interval interval)
        {
            interval.Sets = Int32.Parse(_setsInput.Text);
            interval.RepsMin = Int32.Parse(_repsMinInput.Text);
            interval.RepsMax = Int32.Parse(_repsMaxInput.Text);
            interval.RepDuration = Int32.Parse(_repDurationInput.Text);
            interval.Pause = _pauseInput.Text;
        }

        private Panel CreateRecordPanel(Interval interval)
        {
            var panel = new Panel
            {
                BackColor = Color.LightGray,
                Location = new Point(0, 0),
                Name = "recordPanel",
                Size = new Size(1200, 380),
                TabIndex = 1
            };
            
            var setsLabel = new Label
            {
                Text = "Sets",
                Location = new Point(10, 10),
                Size = new Size(200, 30)
            };
            _setsInput = new IntBox
            {
                Text = interval.Sets.ToString(),
                Location = new Point(220, 10),
                Size = new Size(250, 30),
                TabIndex = 2
            };
            
            var repsMinLabel = new Label
            {
                Text = "min. Reps",
                Location = new Point(10, 50),
                Size = new Size(200, 30)
            };
            _repsMinInput = new IntBox
            {
                Text = interval.RepsMin.ToString(),
                Location = new Point(220, 50),
                Size = new Size(250, 30),
                TabIndex = 3
            };
            
            var repsMaxLabel = new Label
            {
                Text = "max. Reps",
                Location = new Point(10, 90),
                Size = new Size(200, 30)
            };
            _repsMaxInput = new IntBox
            {
                Text = interval.RepsMax.ToString(),
                Location = new Point(220, 90),
                Size = new Size(250, 30),
                TabIndex = 4
            };
            
            var repDurationLabel = new Label
            {
                Text = "Rep Dauer",
                Location = new Point(10, 130),
                Size = new Size(200, 30)
            };
            _repDurationInput = new IntBox
            {
                Text = interval.RepDuration.ToString(),
                Location = new Point(220, 130),
                Size = new Size(250, 30),
                TabIndex = 5
            };
            
            var pauseLabel = new Label
            {
                Text = "Pause",
                Location = new Point(10, 170),
                Size = new Size(200, 30)
            };
            _pauseInput = new TextBox
            {
                Text = interval.Pause,
                Location = new Point(220, 170),
                Size = new Size(250, 30),
                TabIndex = 5
            };
            
            panel.Controls.Add(setsLabel);
            panel.Controls.Add(_setsInput);

            panel.Controls.Add(repsMinLabel);
            panel.Controls.Add(_repsMinInput);
            
            panel.Controls.Add(repsMaxLabel);
            panel.Controls.Add(_repsMaxInput);
            
            panel.Controls.Add(repDurationLabel);
            panel.Controls.Add(_repDurationInput);
            
            panel.Controls.Add(pauseLabel);
            panel.Controls.Add(_pauseInput);

            return panel;
        }
    }
}