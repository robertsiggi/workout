using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Model;

namespace Forms.Controls
{
    public class ExerciseControl: BaseControl, IBaseControl
    {
        private TextBox _exerciseInput;
        private TextBox _descriptionInput;
        private TextBox _externalRef1TextBox;
        private TextBox _externalRef2TextBox;
        private CheckBox _isBodyWeightCheckBox;
        private CheckBox _isCompoundExerciseCheckBox;
        private ComboBox _muscle1ComboBox;
        private ComboBox _muscle2ComboBox;

        public void GetExercises(ListView listView)
        {
            listView.Items.Clear();
            
            var exercises = DatabaseContext.Exercises.ToList();
            
            listView.Items.Add(new ListViewItem("*"));

            foreach (var exercise in exercises)
            {
                var item = new ListViewItem(exercise.Id.ToString());
                item.SubItems.Add(exercise.Name);
                item.SubItems.Add(exercise.Description);
                item.SubItems.Add(exercise.IsBodyWeight.ToString());
                item.SubItems.Add(exercise.IsCompoundExercise.ToString());
                item.SubItems.Add(exercise.Muscle1 != null ? exercise.Muscle1.NameGerman : "");
                item.SubItems.Add(exercise.Muscle2 != null ? exercise.Muscle2.NameGerman : "");
                item.SubItems.Add(exercise.ExternalRef1);
                item.SubItems.Add(exercise.ExternalRef2);

                item.Tag = exercise;

                listView.Items.Add(item);
            }
        }
        
        public bool NewExercise()
        {
            var exercise = new Exercise();
            var panel = CreateRecordPanel(exercise);
            
            var recordForm = new RecordForm(panel);
            var dialogResult = recordForm.ShowDialog();
            if (dialogResult != DialogResult.OK)
            {
                return false;
            }

            PanelToObject(exercise);

            DatabaseContext.Exercises.Add(exercise);
            DatabaseContext.SaveChanges();

            return true;
        }

        public bool EditExercise(Exercise exercise)
        {
            var panel = CreateRecordPanel(exercise);
            
            var recordForm = new RecordForm(panel);
            var dialogResult = recordForm.ShowDialog();
            if (dialogResult != DialogResult.OK)
            {
                return false;
            }

            PanelToObject(exercise);

            DatabaseContext.Exercises.Update(exercise);
            DatabaseContext.SaveChanges();

            return true;
        }

        private void PanelToObject(Exercise exercise)
        {
            exercise.Name = _exerciseInput.Text;
            exercise.Description = _descriptionInput.Text;
            exercise.ExternalRef1 = _externalRef1TextBox.Text;
            exercise.ExternalRef2 = _externalRef2TextBox.Text;
            exercise.IsBodyWeight = _isBodyWeightCheckBox.Checked;
            exercise.IsCompoundExercise = _isCompoundExerciseCheckBox.Checked;

            if (_muscle1ComboBox.SelectedItem != null)
            {
                var muscle1 = DatabaseContext.Muscles.FirstOrDefault(x => x.NameGerman.Equals(_muscle1ComboBox.SelectedItem));
                exercise.Muscle1Id = muscle1.Id;
            }
            else
            {
                exercise.Muscle1Id = null;
                exercise.Muscle1 = null;
            }
            
            if (_muscle2ComboBox.SelectedItem != null)
            {
                var muscle2 = DatabaseContext.Muscles.FirstOrDefault(x => x.NameGerman.Equals(_muscle2ComboBox.SelectedItem));
                exercise.Muscle2Id = muscle2.Id;
            }
            else
            {
                exercise.Muscle2Id = null;
                exercise.Muscle2 = null;
            }
        }

        private Panel CreateRecordPanel(Exercise exercise)
        {
            var panel = new Panel
            {
                BackColor = Color.LightGray,
                Location = new Point(0, 0),
                Name = "recordPanel",
                Size = new Size(1200, 380),
                TabIndex = 1
            };
            
            var exerciseLabel = new Label
            {
                Text = "Übung",
                Location = new Point(10, 10),
                Size = new Size(200, 30)
            };
            _exerciseInput = new TextBox
            {
                Text = exercise.Name,
                Location = new Point(220, 10),
                Size = new Size(600, 30),
                TabIndex = 2
            };
            
            var descriptionLabel = new Label
            {
                Text = "Beschreibung",
                Location = new Point(10, 50),
                Size = new Size(200, 30)
            };
            _descriptionInput = new TextBox
            {
                Text = exercise.Description,
                Location = new Point(220, 50),
                Size = new Size(600, 30),
                TabIndex = 3
            };
            
            var isBodyWeightLabel = new Label
            {
                Text = "Körpergewichtsübung",
                Location = new Point(10, 90),
                Size = new Size(200, 30)
            };
            _isBodyWeightCheckBox = new CheckBox
            {
                Checked = exercise.IsBodyWeight,
                Location = new Point(220, 90),
                Size = new Size(600, 30),
                TabIndex = 4
            };
            
            var isCompoundExerciseLabel = new Label
            {
                Text = "Verbundübung",
                Location = new Point(10, 130),
                Size = new Size(200, 30)
            };
            _isCompoundExerciseCheckBox = new CheckBox
            {
                Checked = exercise.IsCompoundExercise,
                Location = new Point(220, 130),
                Size = new Size(600, 30),
                TabIndex = 5
            };
            
            var externalRef1Label = new Label
            {
                Text = "Verweis 1",
                Location = new Point(10, 170),
                Size = new Size(200, 30)
            };
            _externalRef1TextBox = new TextBox
            {
                Text = exercise.ExternalRef1,
                Location = new Point(220, 170),
                Size = new Size(600, 30),
                TabIndex = 6
            };
            
            var externalRef2Label = new Label
            {
                Text = "Verweis 2",
                Location = new Point(10, 210),
                Size = new Size(200, 30)
            };
            _externalRef2TextBox = new TextBox
            {
                Text = exercise.ExternalRef2,
                Location = new Point(220, 210),
                Size = new Size(600, 30),
                TabIndex = 7
            };

            var muscle1Label = new Label
            {
                Text = "Muskel 1",
                Location = new Point(10, 250),
                Size = new Size(200, 30)
            };
            _muscle1ComboBox = new ComboBox
            {
                Text = exercise.Muscle1?.NameGerman,
                Location = new Point(220, 250),
                Size = new Size(600, 30),
                TabIndex = 8
            };
            
            var muscle2Label = new Label
            {
                Text = "Muskel 2",
                Location = new Point(10, 290),
                Size = new Size(200, 30)
            };
            _muscle2ComboBox = new ComboBox
            {
                Text = exercise.Muscle2?.NameGerman,
                Location = new Point(220, 290),
                Size = new Size(600, 30),
                TabIndex = 9
            };
            
            var muscles = DatabaseContext.Muscles.ToList().Select(x => x.NameGerman).ToArray();
            
            _muscle1ComboBox.Items.Add("");
            _muscle2ComboBox.Items.Add("");
            _muscle1ComboBox.Items.AddRange(muscles);
            _muscle2ComboBox.Items.AddRange(muscles);
            
            panel.Controls.Add(exerciseLabel);
            panel.Controls.Add(_exerciseInput);
            
            panel.Controls.Add(descriptionLabel);
            panel.Controls.Add(_descriptionInput);
            
            panel.Controls.Add(isBodyWeightLabel);
            panel.Controls.Add(_isBodyWeightCheckBox);
            
            panel.Controls.Add(isCompoundExerciseLabel);
            panel.Controls.Add(_isCompoundExerciseCheckBox);
            
            panel.Controls.Add(externalRef1Label);
            panel.Controls.Add(_externalRef1TextBox);
            
            panel.Controls.Add(externalRef2Label);
            panel.Controls.Add(_externalRef2TextBox);
            
            panel.Controls.Add(muscle1Label);
            panel.Controls.Add(_muscle1ComboBox);
            
            panel.Controls.Add(muscle2Label);
            panel.Controls.Add(_muscle2ComboBox);

            return panel;
        }
    }
}