using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Model;

namespace Forms.Controls
{
    public class ProgressControl: BaseControl, IBaseControl
    {
        private ComboBox _exerciseComboBox;
        private ComboBox _intervalComboBox;
        private ComboBox _weightComboBox;
        
        public void GetProgresss(ListView listView)
        {
            listView.Items.Clear();
            
            var progresses = DatabaseContext.Progresses.ToList();
            
            listView.Items.Add(new ListViewItem("*"));
            
            foreach (var progress in progresses)
            {
                var item = new ListViewItem(progress.Id.ToString());
                item.SubItems.Add(progress.Exercise?.Name);
                item.SubItems.Add(progress.Weight?.Name);
                item.SubItems.Add(progress.Interval?.Sets.ToString());
                item.SubItems.Add(progress.Interval?.RepsMin.ToString());
                item.SubItems.Add(progress.Interval?.RepsMax.ToString());
                item.SubItems.Add(progress.Interval?.RepDuration.ToString());
                item.SubItems.Add(progress.Interval?.Pause);

                item.Tag = progress;

                listView.Items.Add(item);
            }
        }
        
        public bool NewProgress()
        {
            var progress = new Progress();
            var panel = CreateRecordPanel(progress);
            
            var recordForm = new RecordForm(panel);
            var dialogResult = recordForm.ShowDialog();
            if (dialogResult != DialogResult.OK)
            {
                return false;
            }

            PanelToObject(progress);

            DatabaseContext.Progresses.Add(progress);
            DatabaseContext.SaveChanges();

            return true;
        }

        public bool EditProgress(Progress progress)
        {
            var panel = CreateRecordPanel(progress);
            
            var recordForm = new RecordForm(panel);
            var dialogResult = recordForm.ShowDialog();
            if (dialogResult != DialogResult.OK)
            {
                return false;
            }

            PanelToObject(progress);

            DatabaseContext.Progresses.Update(progress);
            DatabaseContext.SaveChanges();

            return true;
        }

        private void PanelToObject(Progress progress)
        {
            if (_exerciseComboBox.SelectedItem != null)
            {
                progress.Exercise = DatabaseContext.Exercises.FirstOrDefault(x => x.Name.Equals(_exerciseComboBox.SelectedItem));
                progress.ExerciseId = progress.Exercise.Id;
                progress.Exercise = null;
            }

            if (_weightComboBox.SelectedItem != null)
            {
                progress.Weight = DatabaseContext.Weights.FirstOrDefault(x => x.Name.Equals(_weightComboBox.SelectedItem));
                progress.WeightId = progress.Weight.Id;
                progress.Weight = null;

            }

            if (_intervalComboBox.SelectedItem != null)
            {
                var intervalIdString = _intervalComboBox.SelectedItem.ToString().Substring(_intervalComboBox.SelectedItem.ToString().Length - 36);
                progress.Interval = DatabaseContext.Intervals.Find(new Guid(intervalIdString));
                progress.IntervalId = progress.Interval.Id;
                progress.Interval = null;
            }
        }

        private Panel CreateRecordPanel(Progress progress)
        {
            var panel = new Panel
            {
                BackColor = Color.LightGray,
                Location = new Point(0, 0),
                Name = "recordPanel",
                Size = new Size(1200, 380),
                TabIndex = 1
            };
            
            
            var exerciseLabel = new Label
            {
                Text = "Übung",
                Location = new Point(10, 10),
                Size = new Size(200, 30)
            };
            _exerciseComboBox = new ComboBox
            {
                Text = progress.Exercise?.Name,
                Location = new Point(220, 10),
                Size = new Size(600, 30),
                TabIndex = 2
            };
            
            var intervalLabel = new Label
            {
                Text = "Interval",
                Location = new Point(10, 50),
                Size = new Size(200, 30)
            };
            _intervalComboBox = new ComboBox
            {
                Text = GetIntervalComboItem(progress.Interval),
                Tag = progress.IntervalId,
                Location = new Point(220, 50),
                Size = new Size(600, 30),
                TabIndex = 3
            };
            
            var weightLabel = new Label
            {
                Text = "Gewicht",
                Location = new Point(10, 90),
                Size = new Size(200, 30)
            };
            _weightComboBox = new ComboBox
            {
                Text = progress.Weight?.Name,
                Location = new Point(220, 90),
                Size = new Size(600, 30),
                TabIndex = 4
            };


            var exercises = DatabaseContext.Exercises.ToList();
            var intervals = DatabaseContext.Intervals.ToList();
            var weights = DatabaseContext.Weights.ToList();

            // _exerciseComboBox.Items.Add("");
            // _intervalComboBox.Items.Add("");
            // _weightComboBox.Items.Add("");
            
            foreach (var exercise in exercises)
            {
                _exerciseComboBox.Items.Add(exercise.Name);
            }

            foreach (var interval in intervals)
            {
                _intervalComboBox.Items.Add(GetIntervalComboItem(interval));
            }

            foreach (var weight in weights)
            {
                _weightComboBox.Items.Add(weight.Name);
            }
            
            panel.Controls.Add(exerciseLabel);
            panel.Controls.Add(_exerciseComboBox);
            
            panel.Controls.Add(intervalLabel);
            panel.Controls.Add(_intervalComboBox);
            
            panel.Controls.Add(weightLabel);
            panel.Controls.Add(_weightComboBox);

            return panel;
        }

        private string GetIntervalComboItem(Interval interval)
        {
            if (interval == null)
            {
                return string.Empty;
            }
            
            return interval.Sets + " / " + interval.RepsMin + "-" + interval.RepsMax + " / " + interval.RepDuration + " / " + interval.Pause + " ###" + interval.Id;
        }
    }
}