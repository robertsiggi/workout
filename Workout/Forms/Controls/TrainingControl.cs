using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Model;

namespace Forms.Controls
{
    public class TrainingControl: BaseControl, IBaseControl
    {
        public void GetTrainings(ListView listView)
        {
            listView.Items.Clear();
            
            var trainings = DatabaseContext.Trainings.ToList();
            
            listView.Items.Add(new ListViewItem("*"));
            
            foreach (var training in trainings)
            {
                var item = new ListViewItem(training.Id.ToString());
                item.SubItems.Add(training.Day.ToShortDateString());
                //item.SubItems.Add(training.);
                
                
                /*
                 * TRAINING
                 *                                 --> Day
                 *     TRAININGSET
                 *                                 --> Name
                 *         TRAININGEXERCISE
                 *             EXERCISE
                 *                                 --> Name
                 *             INTERVAL
                 *                                 --> Sets, RepsMin, RepsMax, RepDuration, Pause
                 *             WEIGHT
                 *                                 --> Name
                 */

                item.Tag = training;

                listView.Items.Add(item);
            }
        }
        
        public bool NewTraining()
        {
            var training = new Training();
            var panel = CreateRecordPanel(training);
            
            var recordForm = new RecordForm(panel);
            var dialogResult = recordForm.ShowDialog();
            if (dialogResult != DialogResult.OK)
            {
                return false;
            }

            PanelToObject(training);

            DatabaseContext.Trainings.Add(training);
            DatabaseContext.SaveChanges();

            return true;
        }

        public bool EditTraining(Training training)
        {
            var panel = CreateRecordPanel(training);
            
            var recordForm = new RecordForm(panel);
            var dialogResult = recordForm.ShowDialog();
            if (dialogResult != DialogResult.OK)
            {
                return false;
            }

            PanelToObject(training);

            DatabaseContext.Trainings.Update(training);
            DatabaseContext.SaveChanges();

            return true;
        }

        private void PanelToObject(Training training)
        {
            // training.Name = _trainingInput.Text;
            // training.MinSetsPerWeek = Int32.Parse(_minSetsPerWeekInput.Text);
        }

        private Panel CreateRecordPanel(Training training)
        {
            var panel = new Panel
            {
                BackColor = Color.LightGray,
                Location = new Point(0, 0),
                Name = "recordPanel",
                Size = new Size(1200, 380),
                TabIndex = 1
            };
            
            
            // var trainingLabel = new Label
            // {
            //     Text = "Muskel",
            //     Location = new Point(300, 10),
            //     Size = new Size(200, 30)
            // };
            // _trainingInput = new TextBox
            // {
            //     Text = training.Name,
            //     Location = new Point(510, 10),
            //     Size = new Size(250, 30),
            //     TabIndex = 3
            // };
            //
            // var minSetsPerWeekLabel = new Label
            // {
            //     Text = "Min. Sets pro Woche",
            //     Location = new Point(300, 50),
            //     Size = new Size(200, 30)
            // };
            // _minSetsPerWeekInput = new DecimalBox
            // {
            //     Text = training.MinSetsPerWeek.ToString(),
            //     Location = new Point(510, 50),
            //     Size = new Size(250, 30),
            //     TabIndex = 3
            // };
            
            // panel.Controls.Add(trainingLabel);
            // panel.Controls.Add(_trainingInput);


            return panel;
        }
    }
}