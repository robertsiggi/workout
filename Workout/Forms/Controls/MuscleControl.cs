using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Model;

namespace Forms.Controls
{
    public class MuscleControl: BaseControl, IBaseControl
    {
        private TextBox _muscleInput;

        public MuscleControl()
        {
        }

        public void GetMuscles(ListView listView)
        {
            listView.Items.Clear();
            
            var muscles = DatabaseContext.Muscles.ToList();
            
            listView.Items.Add(new ListViewItem("*"));
            
            foreach (var muscle in muscles)
            {
                var item = new ListViewItem(muscle.Id.ToString());
                item.SubItems.Add(muscle.NameGerman);

                item.Tag = muscle;

                listView.Items.Add(item);
            }
        }
        
        public bool NewMuscle()
        {
            var muscle = new Muscle();
            var panel = CreateRecordPanel(muscle);
            
            var recordForm = new RecordForm(panel);
            var dialogResult = recordForm.ShowDialog();
            if (dialogResult != DialogResult.OK)
            {
                return false;
            }

            PanelToObject(muscle);

            DatabaseContext.Muscles.Add(muscle);
            DatabaseContext.SaveChanges();

            return true;
        }

        public bool EditMuscle(Muscle muscle)
        {
            var panel = CreateRecordPanel(muscle);
            
            var recordForm = new RecordForm(panel);
            var dialogResult = recordForm.ShowDialog();
            if (dialogResult != DialogResult.OK)
            {
                return false;
            }

            PanelToObject(muscle);

            DatabaseContext.Muscles.Update(muscle);
            DatabaseContext.SaveChanges();

            return true;
        }

        private void PanelToObject(Muscle muscle)
        {
            muscle.NameGerman = _muscleInput.Text;
        }

        private Panel CreateRecordPanel(Muscle muscle)
        {
            var panel = new Panel
            {
                BackColor = Color.LightGray,
                Location = new Point(0, 0),
                Name = "recordPanel",
                Size = new Size(1200, 380),
                TabIndex = 1
            };
            
            
            var muscleLabel = new Label
            {
                Text = "Muskel",
                Location = new Point(10, 10),
                Size = new Size(200, 30)
            };
            _muscleInput = new TextBox
            {
                Text = muscle.NameGerman,
                Location = new Point(220, 10),
                Size = new Size(600, 30),
                TabIndex = 3
            };
                        
            panel.Controls.Add(muscleLabel);
            panel.Controls.Add(_muscleInput);

            return panel;
        }
    }
}