using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Model;

namespace Forms.Controls
{
    public class ExerciseSelectionControl: BaseControl, IBaseControl
    {
        private TextBox _exerciseSelectionInput;
        private IntBox _setsPerWeekInput;
        private ComboBox _exercise1ComboBox;
        private ComboBox _exercise2ComboBox;
        private ComboBox _exercise3ComboBox;
        private ComboBox _exercise4ComboBox;
        
        public void GetExerciseSelections(ListView listView)
        {
            listView.Items.Clear();

            var exerciseSelections = DatabaseContext.ExerciseSelections.ToList();
            
            listView.Items.Add(new ListViewItem("*"));
            
            foreach (var exerciseSelection in exerciseSelections)
            {
                var item = new ListViewItem(exerciseSelection.Id.ToString());
                item.SubItems.Add(exerciseSelection.Name);
                item.SubItems.Add(exerciseSelection.SetsPerWeek.ToString());
                foreach (var exercise in exerciseSelection.Exercises)
                {
                    item.SubItems.Add(exercise.Exercise.Name);
                }

                item.Tag = exerciseSelection;

                listView.Items.Add(item);
            }
        }
        
        public bool NewExerciseSelection()
        {
            var exerciseSelection = new ExerciseSelection();
            var panel = CreateRecordPanel(exerciseSelection);
            
            var recordForm = new RecordForm(panel);
            var dialogResult = recordForm.ShowDialog();
            if (dialogResult != DialogResult.OK)
            {
                return false;
            }

            PanelToObject(exerciseSelection);

            DatabaseContext.ExerciseSelections.Add(exerciseSelection);
            DatabaseContext.SaveChanges();

            return true;
        }

        public bool EditExerciseSelection(ExerciseSelection exerciseSelection)
        {
            var panel = CreateRecordPanel(exerciseSelection);
            
            var recordForm = new RecordForm(panel);
            var dialogResult = recordForm.ShowDialog();
            if (dialogResult != DialogResult.OK)
            {
                return false;
            }

            PanelToObject(exerciseSelection);

            DatabaseContext.ExerciseSelections.Update(exerciseSelection);
            DatabaseContext.SaveChanges();

            return true;
        }

        private void PanelToObject(ExerciseSelection exerciseSelection)
        {
            exerciseSelection.Name = _exerciseSelectionInput.Text;
            exerciseSelection.SetsPerWeek = int.Parse(_setsPerWeekInput.Text);

            exerciseSelection.Exercises ??= new List<Exercise2ExerciseSelection>();
            
            var oldExerciseIds = new List<Guid>();
            var newExerciseIds = new List<Guid>();

            if (exerciseSelection.Id != Guid.Empty)
            {
                oldExerciseIds.AddRange(from exerciseId in DatabaseContext.Exercise2ExerciseSelections.ToList().Select(x => x.ExerciseId) where exerciseId != null select (Guid) exerciseId);
            }
            
            if (!string.IsNullOrEmpty(_exercise1ComboBox.Text))
            {
                var exercise = DatabaseContext.Exercises.FirstOrDefault(x =>  x.Name.Equals(_exercise1ComboBox.Text));
                newExerciseIds.Add(exercise.Id);
            }
            
            if (!string.IsNullOrEmpty(_exercise2ComboBox.Text))
            {
                var exercise = DatabaseContext.Exercises.FirstOrDefault(x =>  x.Name.Equals(_exercise2ComboBox.Text));
                newExerciseIds.Add(exercise.Id);
            }
            
            if (!string.IsNullOrEmpty(_exercise3ComboBox.Text))
            {
                var exercise = DatabaseContext.Exercises.FirstOrDefault(x =>  x.Name.Equals(_exercise3ComboBox.Text));
                newExerciseIds.Add(exercise.Id);
            }
            
            if (!string.IsNullOrEmpty(_exercise4ComboBox.Text))
            {
                var exercise = DatabaseContext.Exercises.FirstOrDefault(x =>  x.Name.Equals(_exercise4ComboBox.Text));
                newExerciseIds.Add(exercise.Id);
            }

            // if new object/record --> only new records have to be inserted 
            if (exerciseSelection.Id == Guid.Empty)
            {
                foreach (var newExerciseId in newExerciseIds)
                {
                    var exercise2ExerciseSelection = new Exercise2ExerciseSelection
                    {
                        ExerciseId = newExerciseId,
                        ExerciseSelection = exerciseSelection
                    };
                    exerciseSelection.Exercises.Add(exercise2ExerciseSelection);
                }

                return;
            }
            
            // if existing object/record --> remove old (unused) relation records and insert new ones
            foreach (var oldExerciseId in oldExerciseIds)
            {
                var newExercise = newExerciseIds.Any(x => x.Equals(oldExerciseId)); 
                if (newExercise)
                {
                    continue;
                }

                var oldExercise = DatabaseContext.Exercise2ExerciseSelections.FirstOrDefault(x => x.ExerciseId.Equals(oldExerciseId) && x.ExerciseSelectionId.Equals(exerciseSelection.Id));
                if (oldExercise != null)
                {
                    DatabaseContext.Exercise2ExerciseSelections.Remove(oldExercise);
                }

                oldExercise = exerciseSelection.Exercises.FirstOrDefault(x => x.ExerciseId.Equals(oldExerciseId) && x.ExerciseSelectionId.Equals(exerciseSelection.Id));
                if (oldExercise != null)
                {
                    exerciseSelection.Exercises.Remove(oldExercise);
                }
            }

            foreach (var newExerciseId in newExerciseIds)
            {
                var oldExercise = oldExerciseIds.Any(x => x.Equals(newExerciseId));
                if (oldExercise)
                {
                    continue;
                }
                
                var exercise2ExerciseSelection = new Exercise2ExerciseSelection
                {
                    ExerciseId = newExerciseId,
                    ExerciseSelection = exerciseSelection
                };
                exerciseSelection.Exercises.Add(exercise2ExerciseSelection);
            }
        }

        private Panel CreateRecordPanel(ExerciseSelection exerciseSelection)
        {
            var panel = new Panel
            {
                BackColor = Color.LightGray,
                Location = new Point(0, 0),
                Name = "recordPanel",
                Size = new Size(1200, 380),
                TabIndex = 1
            };
            
            
            var exerciseSelectionLabel = new Label
            {
                Text = "Auswahl",
                Location = new Point(10, 10),
                Size = new Size(200, 30)
            };
            _exerciseSelectionInput = new TextBox
            {
                Text = exerciseSelection.Name,
                Location = new Point(220, 10),
                Size = new Size(600, 30),
                TabIndex = 2
            };
            
            var setsPerWeekLabel = new Label
            {
                Text = "Sets pro Woche",
                Location = new Point(10, 50),
                Size = new Size(200, 30)
            };
            _setsPerWeekInput = new IntBox
            {
                Text = exerciseSelection.SetsPerWeek.ToString(),
                Location = new Point(220, 50),
                Size = new Size(600, 30),
                TabIndex = 3
            };
            
            var exercise1Label = new Label
            {
                Text = "Übung 1",
                Location = new Point(10, 90),
                Size = new Size(200, 30)
            };
            _exercise1ComboBox= new ComboBox()
            {
                Location = new Point(220, 90),
                Size = new Size(600, 30),
                TabIndex = 4
            };
            
            var exercise2Label = new Label
            {
                Text = "Übung 2",
                Location = new Point(10, 130),
                Size = new Size(200, 30)
            };
            _exercise2ComboBox= new ComboBox()
            {
                Location = new Point(220, 130),
                Size = new Size(600, 30),
                TabIndex = 5
            };
            
            var exercise3Label = new Label
            {
                Text = "Übung 3",
                Location = new Point(10, 170),
                Size = new Size(200, 30)
            };
            _exercise3ComboBox= new ComboBox()
            {
                Location = new Point(220, 170),
                Size = new Size(600, 30),
                TabIndex = 6
            };
            
            var exercise4Label = new Label
            {
                Text = "Übung 4",
                Location = new Point(10, 210),
                Size = new Size(200, 30)
            };
            _exercise4ComboBox= new ComboBox()
            {
                Location = new Point(220, 210),
                Size = new Size(600, 30),
                TabIndex = 7
            };

            var exercise2ExerciseSelection = exerciseSelection.Exercises;
            if (exercise2ExerciseSelection != null)
            {
                var cnt = 1;
                foreach (var selection in exercise2ExerciseSelection)
                {
                    var exercise = selection.Exercise;
                    switch (cnt)
                    {
                        case 1:
                            _exercise1ComboBox.Text = exercise.Name;
                            break;
                        case 2:
                            _exercise2ComboBox.Text = exercise.Name;
                            break;
                        case 3:
                            _exercise3ComboBox.Text = exercise.Name;
                            break;
                        case 4:
                            _exercise4ComboBox.Text = exercise.Name;
                            break;
                    }

                    cnt++;
                }
            }

            var exercises = DatabaseContext.Exercises.Select(x => x.Name).ToArray();
            
            _exercise1ComboBox.Items.Add("");
            _exercise2ComboBox.Items.Add("");
            _exercise3ComboBox.Items.Add("");
            _exercise4ComboBox.Items.Add("");
            
            _exercise1ComboBox.Items.AddRange(exercises);
            _exercise2ComboBox.Items.AddRange(exercises);
            _exercise3ComboBox.Items.AddRange(exercises);
            _exercise4ComboBox.Items.AddRange(exercises);
            
            panel.Controls.Add(exerciseSelectionLabel);
            panel.Controls.Add(_exerciseSelectionInput);
            
            panel.Controls.Add(setsPerWeekLabel);
            panel.Controls.Add(_setsPerWeekInput);
            
            panel.Controls.Add(exercise1Label);
            panel.Controls.Add(_exercise1ComboBox);
            
            panel.Controls.Add(exercise2Label);
            panel.Controls.Add(_exercise2ComboBox);
            
            panel.Controls.Add(exercise3Label);
            panel.Controls.Add(_exercise3ComboBox);
            
            panel.Controls.Add(exercise4Label);
            panel.Controls.Add(_exercise4ComboBox);

            return panel;
        }
    }
}