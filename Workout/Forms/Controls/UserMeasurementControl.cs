using System;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Model;

namespace Forms.Controls
{
    public class UserMeasurementControl: BaseControl, IBaseControl
    {
        private DateTimePicker _dateInput;
        private TextBox _weightInput;
        private TextBox _shoulderInput;
        private TextBox _upperArmLeftInput;
        private TextBox _upperArmRightInput;
        private TextBox _foreArmLeftInput;
        private TextBox _foreArmRightInput;
        private TextBox _chestInput;
        private TextBox _waistInput;
        private TextBox _thighLeftInput;
        private TextBox _thighRightInput;
        private TextBox _calfLeftInput;
        private TextBox _calfRightInput;


        public void GetUserMeasurements(ListView listView)
        {
            listView.Items.Clear();

            var userMeasurements = DatabaseContext.UserMeasurements.ToList();
            
            listView.Items.Add(new ListViewItem("*"));
            
            foreach (var userMeasurement in userMeasurements)
            {
                var item = new ListViewItem(userMeasurement.Id.ToString());
                item.SubItems.Add(userMeasurement.Date.ToString("yyyy-MM-dd"));
                item.SubItems.Add(userMeasurement.Weight.ToString());
                item.SubItems.Add(userMeasurement.Shoulder.ToString());
                item.SubItems.Add(userMeasurement.UpperArmLeft.ToString());
                item.SubItems.Add(userMeasurement.UpperArmRight.ToString());
                item.SubItems.Add(userMeasurement.ForeArmLeft.ToString());
                item.SubItems.Add(userMeasurement.ForeArmRight.ToString());
                item.SubItems.Add(userMeasurement.Chest.ToString());
                item.SubItems.Add(userMeasurement.Waist.ToString());
                item.SubItems.Add(userMeasurement.ThighLeft.ToString());
                item.SubItems.Add(userMeasurement.ThighRight.ToString());
                item.SubItems.Add(userMeasurement.CalfLeft.ToString());
                item.SubItems.Add(userMeasurement.CalfRight.ToString());

                item.Tag = userMeasurement;

                listView.Items.Add(item);
            }
        }

        public bool NewUserMeasurement()
        {
            var userMeasurement = new UserMeasurement();
            var panel = CreateRecordPanel(userMeasurement);
            
            var recordForm = new RecordForm(panel);
            var dialogResult = recordForm.ShowDialog();
            if (dialogResult != DialogResult.OK)
            {
                return false;
            }

            PanelToObject(userMeasurement);

            DatabaseContext.UserMeasurements.Add(userMeasurement);
            DatabaseContext.SaveChanges();

            return true;
        }

        public bool EditUserMeasurement(UserMeasurement userMeasurement)
        {
            var panel = CreateRecordPanel(userMeasurement);
            
            var recordForm = new RecordForm(panel);
            var dialogResult = recordForm.ShowDialog();
            if (dialogResult != DialogResult.OK)
            {
                return false;
            }

            PanelToObject(userMeasurement);

            DatabaseContext.UserMeasurements.Update(userMeasurement);
            DatabaseContext.SaveChanges();

            return true;
        }

        private void PanelToObject(UserMeasurement userMeasurement)
        {
            userMeasurement.Date = _dateInput.Value;
            userMeasurement.Weight = float.Parse(_weightInput.Text, NumberStyles.Float);
            userMeasurement.Shoulder = float.Parse(_shoulderInput.Text, NumberStyles.Float);
            userMeasurement.UpperArmLeft = float.Parse(_upperArmLeftInput.Text, NumberStyles.Float);
            userMeasurement.UpperArmRight = float.Parse(_upperArmRightInput.Text, NumberStyles.Float);
            userMeasurement.Chest = float.Parse(_chestInput.Text, NumberStyles.Float);
            userMeasurement.ForeArmLeft = float.Parse(_foreArmLeftInput.Text, NumberStyles.Float);
            userMeasurement.ForeArmRight = float.Parse(_foreArmRightInput.Text, NumberStyles.Float);
            userMeasurement.Waist = float.Parse(_waistInput.Text, NumberStyles.Float);
            userMeasurement.ThighLeft = float.Parse(_thighLeftInput.Text, NumberStyles.Float);
            userMeasurement.ThighRight = float.Parse(_thighRightInput.Text, NumberStyles.Float);
            userMeasurement.CalfLeft = float.Parse(_calfLeftInput.Text, NumberStyles.Float);
            userMeasurement.CalfRight = float.Parse(_calfRightInput.Text, NumberStyles.Float);
        }

        private Panel CreateRecordPanel(UserMeasurement userMeasurement)
        {
            var panel = new Panel
            {
                BackColor = Color.LightGray,
                // Dock = DockStyle.Fill,
                Location = new Point(0, 0),
                Name = "recordPanel",
                Size = new Size(1200, 380),
                TabIndex = 1
            };
            
            /*
 *             Date
 *
*                     Shoulder
 *         UpperArmLeft        UpperArmRight
 *                     Chest
 *         ForeArmLeft        ForeArmRigh
*                     Waist
 *         ThighLeft            ThighRight
 *         CalfLeft            CalfRight
*/
            
            var dateLabel = new Label
            {
                Text = "Erfassungsdatum",
                Location = new Point(300, 10),
                Size = new Size(200, 30)
            };
            var dateInputValue = userMeasurement.Date;
            if (dateInputValue <= new DateTime(2020, 1, 1))
                dateInputValue = DateTime.Today;
            _dateInput = new DateTimePicker
            {
                Value = dateInputValue,
                ShowUpDown = true,
                Format = DateTimePickerFormat.Short,
                Location = new Point(510, 10),
                Size = new Size(250, 30),
                TabIndex = 2
            };
            
            var shoulderLabel = new Label
            {
                Text = "Schultern",
                Location = new Point(300, 50),
                Size = new Size(200, 30)
            };
            _shoulderInput = new DecimalBox
            {
                Text = userMeasurement.Shoulder.ToString(),
                Location = new Point(510, 50),
                Size = new Size(250, 30),
                TabIndex = 3
            };
            
            var upperArmLeftLabel = new Label
            {
                Text = "Linker Oberarm",
                Location = new Point(10, 90),
                Size = new Size(200, 30)
            };
            _upperArmLeftInput = new DecimalBox
            {
                Text = userMeasurement.UpperArmLeft.ToString(),
                Location = new Point(220, 90),
                Size = new Size(250, 30),
                TabIndex = 4
            };
            
            var upperArmRightLabel = new Label
            {
                Text = "Rechter Oberarm",
                Location = new Point(650, 90),
                Size = new Size(200, 30)
            };
            _upperArmRightInput = new DecimalBox
            {
                Text = userMeasurement.UpperArmRight.ToString(),
                Location = new Point(860, 90),
                Size = new Size(250, 30),
                TabIndex = 5
            };
            
            var chestLabel = new Label
            {
                Text = "Brust",
                Location = new Point(300, 130),
                Size = new Size(200, 30)
            };
            _chestInput = new DecimalBox
            {
                Text = userMeasurement.Chest.ToString(),
                Location = new Point(510, 130),
                Size = new Size(250, 30),
                TabIndex = 6
            };
            
            var foreArmLeftLabel = new Label
            {
                Text = "Linker Unterarm",
                Location = new Point(10, 170),
                Size = new Size(200, 30)
            };
            _foreArmLeftInput = new DecimalBox
            {
                Text = userMeasurement.ForeArmLeft.ToString(),
                Location = new Point(220, 170),
                Size = new Size(250, 30),
                TabIndex = 7
            };
            
            var foreArmRightLabel = new Label
            {
                Text = "Rechter Unterarm",
                Location = new Point(650, 170),
                Size = new Size(200, 30)
            };
            _foreArmRightInput = new DecimalBox
            {
                Text = userMeasurement.ForeArmRight.ToString(),
                Location = new Point(860, 170),
                Size = new Size(250, 30),
                TabIndex = 8
            };
            
            var waistLabel = new Label
            {
                Text = "Taille",
                Location = new Point(300, 210),
                Size = new Size(200, 30)
            };
            _waistInput = new DecimalBox
            {
                Text = userMeasurement.Waist.ToString(),
                Location = new Point(510, 210),
                Size = new Size(250, 30),
                TabIndex = 9
            };
            
            var thighLeftLabel = new Label
            {
                Text = "Linker Oberschenkel",
                Location = new Point(10, 250),
                Size = new Size(200, 30)
            };
            _thighLeftInput = new DecimalBox
            {
                Text = userMeasurement.ThighLeft.ToString(),
                Location = new Point(210, 250),
                Size = new Size(250, 30),
                TabIndex = 10
            };
            
            var thighRightLabel = new Label
            {
                Text = "Rechter Oberschenkel",
                Location = new Point(650, 250),
                Size = new Size(200, 30)
            };
            _thighRightInput = new DecimalBox
            {
                Text = userMeasurement.ThighRight.ToString(),
                Location = new Point(860, 250),
                Size = new Size(250, 30),
                TabIndex = 11
            };
            
            var calfLeftLabel = new Label
            {
                Text = "Linker Unterschenkel",
                Location = new Point(10, 290),
                Size = new Size(200, 30)
            };
            _calfLeftInput = new DecimalBox
            {
                Text = userMeasurement.CalfLeft.ToString(),
                Location = new Point(210, 290),
                Size = new Size(250, 30),
                TabIndex = 12
            };
            
            var calfRightLabel = new Label
            {
                Text = "Rechter Unterschenkel",
                Location = new Point(650, 290),
                Size = new Size(200, 30)
            };
            _calfRightInput = new DecimalBox
            {
                Text = userMeasurement.CalfRight.ToString(),
                Location = new Point(860, 290),
                Size = new Size(250, 30),
                TabIndex = 13
            };
            
            var weightLabel = new Label
            {
                Text = "Gewicht",
                Location = new Point(300, 330),
                Size = new Size(200, 30)
            };
            _weightInput = new DecimalBox
            {
                Text = userMeasurement.Weight.ToString(),
                Location = new Point(510, 330),
                Size = new Size(250, 30),
                TabIndex = 14
            };

            panel.Controls.Add(dateLabel);
            panel.Controls.Add(_dateInput);

            panel.Controls.Add(upperArmLeftLabel);
            panel.Controls.Add(_upperArmLeftInput);

            panel.Controls.Add(upperArmRightLabel);
            panel.Controls.Add(_upperArmRightInput);

            panel.Controls.Add(foreArmLeftLabel);
            panel.Controls.Add(_foreArmLeftInput);

            panel.Controls.Add(foreArmRightLabel);
            panel.Controls.Add(_foreArmRightInput);
            
            panel.Controls.Add(chestLabel);
            panel.Controls.Add(_chestInput);
            
            panel.Controls.Add(thighLeftLabel);
            panel.Controls.Add(_thighLeftInput);
            
            panel.Controls.Add(thighRightLabel);
            panel.Controls.Add(_thighRightInput);
            
            panel.Controls.Add(calfLeftLabel);
            panel.Controls.Add(_calfLeftInput);
            
            panel.Controls.Add(calfRightLabel);
            panel.Controls.Add(_calfRightInput);
            
            panel.Controls.Add(waistLabel);
            panel.Controls.Add(_waistInput);
            
            panel.Controls.Add(shoulderLabel);
            panel.Controls.Add(_shoulderInput);
            
            panel.Controls.Add(weightLabel);
            panel.Controls.Add(_weightInput);

            return panel;
        }
    }
}