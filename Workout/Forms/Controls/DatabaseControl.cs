using System;
using System.Windows.Forms;
using Database;

namespace Forms.Controls
{
    public class DatabaseControl: BaseControl, IBaseControl
    {
        private Action _afterDatabaseLoadingAction;
        public void LoadDatabase(string databaseFilePath)
        {
            var connectionString = "Data Source=" + databaseFilePath;
            DatabaseContext = DatabaseContextFactory.CreateDatabaseContext(connectionString);
            _afterDatabaseLoadingAction?.Invoke();
        }

        public string SelectDatabase()
        {
            using (var dialog = new OpenFileDialog())
            {
                dialog.Multiselect = false;
                dialog.Title = "Datenbankdatei auswählen";

                if (dialog.ShowDialog() != DialogResult.OK)
                {
                    return string.Empty;
                }

                var databaseFilePath = dialog.FileName;
                return databaseFilePath;
            }
        }

        public void AfterDatabaseLoadingEvent(Action action)
        {
            _afterDatabaseLoadingAction = action;
        }
    }
}