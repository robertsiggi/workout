using System.Windows.Forms;

namespace Forms
{
    public class DecimalBox: TextBox
    {
        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            if (e.KeyChar == '.')
            {
                e.KeyChar = ',';
            }
            
            if (!char.IsNumber(e.KeyChar) && (Keys)e.KeyChar != Keys.Back && e.KeyChar != ',')
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.' )
            {
                if (Text.Length == 0)
                {
                    Text = "0,";
                    SelectionStart = 2;
                    e.Handled = true;
                }
                else if (Text.Contains(","))
                {
                    e.Handled = true;
                }
            }
            
            base.OnKeyPress(e);
        }
    }
}