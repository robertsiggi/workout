﻿using Model;

namespace Console.Seeds
{
    internal partial class Seeds
    {
        internal void Pause()
        {
            System.Console.WriteLine("Pause seeds");

            DatabaseContext.Pauses.Add(new Pause { Value = "lang" });
            DatabaseContext.Pauses.Add(new Pause { Value = "1 min" });

            DatabaseContext.SaveChanges();

            System.Console.WriteLine("Success");
        }
    }
}
