﻿using Model;
using System.Linq;

namespace Console.Seeds
{
    internal partial class Seeds
    {
        internal void Intervals()
        {
            System.Console.WriteLine("Interval seeds");

            var pauseLong = DatabaseContext.Pauses.Single(x => x.Value.Equals("lang"));
            var pause1min = DatabaseContext.Pauses.Single(x => x.Value.Equals("1 min"));

            DatabaseContext.Intervals.Add(new Interval { Sets = 1, RepsMin = 1, RepsMax = 1, RepDuration = 0, ExerciseNo = 1, PauseId = pauseLong.Id });

            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 3, RepsMax = 5, RepDuration = 0, ExerciseNo = 1, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 3, RepsMax = 5, RepDuration = 0, ExerciseNo = 1, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 6, RepsMax = 8, RepDuration = 0, ExerciseNo = 1, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 6, RepsMax = 8, RepDuration = 0, ExerciseNo = 1, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 8, RepsMax = 10, RepDuration = 0, ExerciseNo = 1, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 8, RepsMax = 10, RepDuration = 0, ExerciseNo = 1, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 10, RepsMax = 12, RepDuration = 0, ExerciseNo = 1, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 10, RepsMax = 12, RepDuration = 0, ExerciseNo = 1, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 12, RepsMax = 15, RepDuration = 0, ExerciseNo = 1, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 12, RepsMax = 15, RepDuration = 0, ExerciseNo = 1, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 15, RepsMax = 20, RepDuration = 0, ExerciseNo = 1, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 15, RepsMax = 20, RepDuration = 0, ExerciseNo = 1, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 20, RepsMax = 25, RepDuration = 0, ExerciseNo = 1, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 20, RepsMax = 25, RepDuration = 0, ExerciseNo = 1, PauseId = pauseLong.Id });

            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 3, RepsMax = 5, RepDuration = 0, ExerciseNo = 2, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 3, RepsMax = 5, RepDuration = 0, ExerciseNo = 2, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 6, RepsMax = 8, RepDuration = 0, ExerciseNo = 2, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 6, RepsMax = 8, RepDuration = 0, ExerciseNo = 2, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 8, RepsMax = 10, RepDuration = 0, ExerciseNo = 2, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 8, RepsMax = 10, RepDuration = 0, ExerciseNo = 2, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 10, RepsMax = 12, RepDuration = 0, ExerciseNo = 2, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 10, RepsMax = 12, RepDuration = 0, ExerciseNo = 2, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 12, RepsMax = 15, RepDuration = 0, ExerciseNo = 2, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 12, RepsMax = 15, RepDuration = 0, ExerciseNo = 2, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 15, RepsMax = 20, RepDuration = 0, ExerciseNo = 2, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 15, RepsMax = 20, RepDuration = 0, ExerciseNo = 2, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 20, RepsMax = 25, RepDuration = 0, ExerciseNo = 2, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 20, RepsMax = 25, RepDuration = 0, ExerciseNo = 2, PauseId = pauseLong.Id });

            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 3, RepsMax = 5, RepDuration = 0, ExerciseNo = 3, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 3, RepsMax = 5, RepDuration = 0, ExerciseNo = 3, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 6, RepsMax = 8, RepDuration = 0, ExerciseNo = 3, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 6, RepsMax = 8, RepDuration = 0, ExerciseNo = 3, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 8, RepsMax = 10, RepDuration = 0, ExerciseNo = 3, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 8, RepsMax = 10, RepDuration = 0, ExerciseNo = 3, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 10, RepsMax = 12, RepDuration = 0, ExerciseNo = 3, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 10, RepsMax = 12, RepDuration = 0, ExerciseNo = 3, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 12, RepsMax = 15, RepDuration = 0, ExerciseNo = 3, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 12, RepsMax = 15, RepDuration = 0, ExerciseNo = 3, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 15, RepsMax = 20, RepDuration = 0, ExerciseNo = 3, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 15, RepsMax = 20, RepDuration = 0, ExerciseNo = 3, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 20, RepsMax = 25, RepDuration = 0, ExerciseNo = 3, PauseId = pauseLong.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 20, RepsMax = 25, RepDuration = 0, ExerciseNo = 3, PauseId = pauseLong.Id });

            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 3, RepsMax = 5, RepDuration = 7, ExerciseNo = 1, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 3, RepsMax = 5, RepDuration = 7, ExerciseNo = 1, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 6, RepsMax = 8, RepDuration = 7, ExerciseNo = 1, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 6, RepsMax = 8, RepDuration = 7, ExerciseNo = 1, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 8, RepsMax = 10, RepDuration = 7, ExerciseNo = 1, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 8, RepsMax = 10, RepDuration = 7, ExerciseNo = 1, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 10, RepsMax = 12, RepDuration = 7, ExerciseNo = 1, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 10, RepsMax = 12, RepDuration = 7, ExerciseNo = 1, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 12, RepsMax = 15, RepDuration = 7, ExerciseNo = 1, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 12, RepsMax = 15, RepDuration = 7, ExerciseNo = 1, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 15, RepsMax = 20, RepDuration = 7, ExerciseNo = 1, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 15, RepsMax = 20, RepDuration = 7, ExerciseNo = 1, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 20, RepsMax = 25, RepDuration = 7, ExerciseNo = 1, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 20, RepsMax = 25, RepDuration = 7, ExerciseNo = 1, PauseId = pause1min.Id });

            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 3, RepsMax = 5, RepDuration = 7, ExerciseNo = 2, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 3, RepsMax = 5, RepDuration = 7, ExerciseNo = 2, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 6, RepsMax = 8, RepDuration = 7, ExerciseNo = 2, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 6, RepsMax = 8, RepDuration = 7, ExerciseNo = 2, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 8, RepsMax = 10, RepDuration = 7, ExerciseNo = 2, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 8, RepsMax = 10, RepDuration = 7, ExerciseNo = 2, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 10, RepsMax = 12, RepDuration = 7, ExerciseNo = 2, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 10, RepsMax = 12, RepDuration = 7, ExerciseNo = 2, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 12, RepsMax = 15, RepDuration = 7, ExerciseNo = 2, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 12, RepsMax = 15, RepDuration = 7, ExerciseNo = 2, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 15, RepsMax = 20, RepDuration = 7, ExerciseNo = 2, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 15, RepsMax = 20, RepDuration = 7, ExerciseNo = 2, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 20, RepsMax = 25, RepDuration = 7, ExerciseNo = 2, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 20, RepsMax = 25, RepDuration = 7, ExerciseNo = 2, PauseId = pause1min.Id });

            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 3, RepsMax = 5, RepDuration = 7, ExerciseNo = 3, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 3, RepsMax = 5, RepDuration = 7, ExerciseNo = 3, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 6, RepsMax = 8, RepDuration = 7, ExerciseNo = 3, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 6, RepsMax = 8, RepDuration = 7, ExerciseNo = 3, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 8, RepsMax = 10, RepDuration = 7, ExerciseNo = 3, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 8, RepsMax = 10, RepDuration = 7, ExerciseNo = 3, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 10, RepsMax = 12, RepDuration = 7, ExerciseNo = 3, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 10, RepsMax = 12, RepDuration = 7, ExerciseNo = 3, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 12, RepsMax = 15, RepDuration = 7, ExerciseNo = 3, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 12, RepsMax = 15, RepDuration = 7, ExerciseNo = 3, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 15, RepsMax = 20, RepDuration = 7, ExerciseNo = 3, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 15, RepsMax = 20, RepDuration = 7, ExerciseNo = 3, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 3, RepsMin = 20, RepsMax = 25, RepDuration = 7, ExerciseNo = 3, PauseId = pause1min.Id });
            DatabaseContext.Intervals.Add(new Interval { Sets = 4, RepsMin = 20, RepsMax = 25, RepDuration = 7, ExerciseNo = 3, PauseId = pause1min.Id });

            DatabaseContext.SaveChanges();

            System.Console.WriteLine("Success");
        }
    }
}
