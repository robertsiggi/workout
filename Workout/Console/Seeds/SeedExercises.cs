﻿using Model;

namespace Console.Seeds
{
    internal partial class Seeds
    {
        internal void Exercises()
        {
            System.Console.WriteLine("Exercise seeds");

            //var DatabaseContext.Exercises = unitOfWork.ExerciseRepository;
            //var repoMuscles = unitOfWork.MuscleRepository;

            //// Brust
            //var muscleObereBrust = repoMuscles.First(x => x.MuscleGerman.Equals("obere Brust"));
            //var muscleBrust = repoMuscles.First(x => x.MuscleGerman.Equals("Brust"));
            //var muscleUntereBrust = repoMuscles.First(x => x.MuscleGerman.Equals("untere Brust"));

            //// Schultern
            //var muscleVordereSchulter = repoMuscles.First(x => x.MuscleGerman.Equals("vordere Schulter"));
            //var muscleMittlereSchulter = repoMuscles.First(x => x.MuscleGerman.Equals("mittlere Schulter"));
            //var muscleHintereSchulter = repoMuscles.First(x => x.MuscleGerman.Equals("hintere Schulter"));

            //// Rücken
            //var muscleBreiterRückenmuskel = repoMuscles.First(x => x.MuscleGerman.Equals("breiter Rückenmuskel"));
            //var muscleRautenmuskel = repoMuscles.First(x => x.MuscleGerman.Equals("Rautenmuskel"));
            //var muscleGroßerRundmuskel = repoMuscles.First(x => x.MuscleGerman.Equals("großer Rundmuskel"));
            //var muscleUntererRückenstrecker = repoMuscles.First(x => x.MuscleGerman.Equals("unterer Rückenstrecker"));

            //// Trapez
            //var muscleObererTrapez = repoMuscles.First(x => x.MuscleGerman.Equals("oberer Trapez"));
            //var muscleMittlererTrapez = repoMuscles.First(x => x.MuscleGerman.Equals("mittlerer Trapez"));
            //var muscleUntererTrapez = repoMuscles.First(x => x.MuscleGerman.Equals("unterer Trapez"));

            //// Trizeps
            //var muscleTrizepsSeitlicherKopf = repoMuscles.First(x => x.MuscleGerman.Equals("seitlicher Trizeps-Kopf"));
            //var muscleTrizepsLangerKopf = repoMuscles.First(x => x.MuscleGerman.Equals("langer Trizeps-Kopf"));
            //var muscleTrizepsMittlererKopf = repoMuscles.First(x => x.MuscleGerman.Equals("mittlerer Trizeps-Kopf"));

            //// Bizeps
            //var muscleBizepsLangerKopf = repoMuscles.First(x => x.MuscleGerman.Equals("langer Bizeps-Kopf"));
            //var muscleBizepsKurzerKopf = repoMuscles.First(x => x.MuscleGerman.Equals("kurzer Bizeps-Kopf"));
            //var muscleBizepsArmbeuger = repoMuscles.First(x => x.MuscleGerman.Equals("Armbeuger"));

            //// Unterarme
            //var muscleUnterarmbeuger = repoMuscles.First(x => x.MuscleGerman.Equals("Unterarmbeuger"));
            //var muscleUnterarmStrecker = repoMuscles.First(x => x.MuscleGerman.Equals("Unterarmstrecker"));

            //// Quadrizeps
            //var muscleAeußererOberschenkelmuskel = repoMuscles.First(x => x.MuscleGerman.Equals("äußerer Oberschenkelmuskel"));
            //var muscleInnererOberschenkelmuskel = repoMuscles.First(x => x.MuscleGerman.Equals("innerer Oberschenkelmuskel"));
            //var muscleMittlererOberschenkelmuskel = repoMuscles.First(x => x.MuscleGerman.Equals("mittlerer Oberschenkelmuskel"));
            //var muscleGeraderOberschenkelmuskel = repoMuscles.First(x => x.MuscleGerman.Equals("gerader Oberschenkelmuskel"));

            //// Hinterer Oberschenkel
            //var muscleBeinbeuger = repoMuscles.First(x => x.MuscleGerman.Equals("Beinbeuger"));
            //var muscleSemitendinosus = repoMuscles.First(x => x.MuscleLatin.Equals("M. semitendinosus"));
            //var muscleSemimembranosus = repoMuscles.First(x => x.MuscleLatin.Equals("M. semimembranosus"));

            //// Gesäß
            //var musclegGroßerGesaessmuskel = repoMuscles.First(x => x.MuscleGerman.Equals("großer Gesäßmuskel"));
            //var muscleMittlererGesaessmuskel = repoMuscles.First(x => x.MuscleGerman.Equals("mittlerer Gesäßmuskel"));

            //// Waden
            //var muscleZwillingswadenmuskel = repoMuscles.First(x => x.MuscleGerman.Equals("Zwillingswadenmuskel"));
            //var muscleSchollenmuskel = repoMuscles.First(x => x.MuscleGerman.Equals("Schollenmuskel"));
            //var muscleVordererSchienbeinmuskel = repoMuscles.First(x => x.MuscleGerman.Equals("vorderer Schienbeinmuskel"));

            //// Bauch und Körpermitte
            //var muscleGeraderBauchmuskel = repoMuscles.First(x => x.MuscleGerman.Equals("gerader Bauchmuskel"));
            //var muscleAeußererSchraegerBauchmuskel = repoMuscles.First(x => x.MuscleGerman.Equals("äußerer schräger Bauchmuskel"));
            //var muscleInnererSchraegerBauchmuskel = repoMuscles.First(x => x.MuscleGerman.Equals("innerer schräger Bauchmuskel"));
            //var muscleQuererBauchmuskel = repoMuscles.First(x => x.MuscleGerman.Equals("querer Bauchmuskel"));


            // Brust
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Bankdrücken", Description = "", ExternalRef1 = "487", IsBodyWeight = false, IsCompoundExercise = true });
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Schrägbankdrücken", Description = "Auf 1. Stufe über flach", ExternalRef1 = "488", IsBodyWeight = true, IsCompoundExercise = true });
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Negativbankdrücken", Description = "", ExternalRef1 = "489", IsBodyWeight = false, IsCompoundExercise = true });
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Drücken auf dem Boden", Description = "", ExternalRef1 = "491", IsBodyWeight = false, IsCompoundExercise = true });
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Bankdrücken im Untergriff", Description = "", ExternalRef1 = "", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Bankdrücken", Description = "", ExternalRef1 = "499", IsBodyWeight = false, IsCompoundExercise = true });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Schrägbankdrücken", Description = "Auf 1. Stufe über flach", ExternalRef1 = "501", IsBodyWeight = false, IsCompoundExercise = true });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Negativbankdrücken", Description = "", ExternalRef1 = "502", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Bankdrücken mit einem Arm", Description = "", ExternalRef1 = "503", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Bankdrücken im neutralen Griff", Description = "Hammergriff", ExternalRef1 = "505", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Bankdrücken im Untergriff", Description = "", ExternalRef1 = "505", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Flys Flachbank", Description = "", ExternalRef1 = "513", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Flys Schrägbank", Description = "Auf 1. Stufe über flach", ExternalRef1 = "514", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Flys Negativbank", Description = "", ExternalRef1 = "515", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Liegestütz", Description = "", ExternalRef1 = "524", IsBodyWeight = true, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Schräger Liegestütz", Description = "", ExternalRef1 = "525", IsBodyWeight = true, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Negativer Liegestütz", Description = "", ExternalRef1 = "526", IsBodyWeight = true, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Powerliegestütze", Description = "", ExternalRef1 = "528", IsBodyWeight = true, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Liegestütze mit Schlingentrainer", Description = "", ExternalRef1 = "", IsBodyWeight = true, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Schräge Liegestütze mit Schlingentrainer", Description = "", ExternalRef1 = "", IsBodyWeight = true, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Negative Liegestütze mit Schlingentrainer", Description = "", ExternalRef1 = "", IsBodyWeight = true, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Brustdips", Description = "", ExternalRef1 = "530", IsBodyWeight = false, IsCompoundExercise = true });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Überzüge", Description = "", ExternalRef1 = "531", IsBodyWeight = false, IsCompoundExercise = false });

            // Schultern
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Überkopfdrücken im Sitzen", Description = "90 Grad", ExternalRef1 = "536", IsBodyWeight = false, IsCompoundExercise = true });
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Überkopfdrücken im Sitzen", Description = "1 Stufe vor 90 Grad", ExternalRef1 = "536", IsBodyWeight = false, IsCompoundExercise = true });
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Nackendrücken im Sitzen", Description = "", ExternalRef1 = "537", IsBodyWeight = false, IsCompoundExercise = true });
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Überkopfdrücken im Stehen", Description = "", ExternalRef1 = "538", IsBodyWeight = false, IsCompoundExercise = true });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Überkopfdrücken im Sitzen", Description = "", ExternalRef1 = "542", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Überkopfdrücken im Sitzen im neutralen Griff", Description = "Hammergriff", ExternalRef1 = "543", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Arnold-Presse im Sitzen", Description = "", ExternalRef1 = "544", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Überkopfdrücken im Stehen", Description = "", ExternalRef1 = "545", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Liegestütz im umgekehrten V", Description = "", ExternalRef1 = "548", IsBodyWeight = true, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Aufrechtes Rudern", Description = "", ExternalRef1 = "550", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Aufrechtes Rudern", Description = "", ExternalRef1 = "551", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "ẞH Aufrechtes Rudern", Description = "", ExternalRef1 = "", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Seitheben im Stehen", Description = "", ExternalRef1 = "565", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Seitheben im Sitzen", Description = "", ExternalRef1 = "567", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Seitheben im Stehen 1/3", Description = "nur erstes Drittel der vollen Bewegung", ExternalRef1 = "565", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Seitheben im Sitzen 1/3", Description = "nur erstes Drittel der vollen Bewegung", ExternalRef1 = "567", IsBodyWeight = false, IsCompoundExercise = false });

            // Schultern vorne
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Frontheben im Stehen", Description = "", ExternalRef1 = "556", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Frontheben im Stehen", Description = "", ExternalRef1 = "557", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Frontheben im Stehen mit einem Arm", Description = "", ExternalRef1 = "558", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "ẞH Frontheben im Stehen", Description = "", ExternalRef1 = "", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Scheibe Frontheben im Stehen", Description = "", ExternalRef1 = "559", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Frontheben im Sitzen Rückenlage", Description = "45 Grad", ExternalRef1 = "562", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "ẞH Frontheben im Sitzen Rückenlage", Description = "45 Grad", ExternalRef1 = "562", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Frontheben im Sitzen Bauchlage", Description = "45 Grad", ExternalRef1 = "563", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "ẞH Frontheben im Sitzen Bauchlage", Description = "45 Grad", ExternalRef1 = "563", IsBodyWeight = false, IsCompoundExercise = false });

            // Schultern hinten
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Vorgebeugtes Seitheben im Stehen", Description = "", ExternalRef1 = "572", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Vorgebeugtes Seitheben im Sitzen Bauchlage", Description = "45 Grad", ExternalRef1 = "574", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Seitheben im Liegen", Description = "Auf der Seite liegend", ExternalRef1 = "577", IsBodyWeight = false, IsCompoundExercise = false });

            // Rücken
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Rudern im Obergriff", Description = "", ExternalRef1 = "590", IsBodyWeight = false, IsCompoundExercise = true  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Rudern im Untergriff", Description = "", ExternalRef1 = "591", IsBodyWeight = false, IsCompoundExercise = true  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Rudern im stehen", Description = "", ExternalRef1 = "601", IsBodyWeight = false, IsCompoundExercise = false  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Rudern einarmig", Description = "602", ExternalRef1 = "", IsBodyWeight = false, IsCompoundExercise = false  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Rudern auf der Schrägbank Bauchlage", Description = "45 Grad", ExternalRef1 = "603", IsBodyWeight = false, IsCompoundExercise = false  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Klimmzug weiter Griff", Description = "", ExternalRef1 = "612", IsBodyWeight = true, IsCompoundExercise = true  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Klimmzug im Hammergriff", Description = "", ExternalRef1 = "612", IsBodyWeight = true, IsCompoundExercise = true  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Klimmzug im Untergriff", Description = "", ExternalRef1 = "613", IsBodyWeight = true, IsCompoundExercise = true });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Horizontales Rudern mit Dipstation", Description = "", ExternalRef1 = "619", IsBodyWeight = true, IsCompoundExercise = false  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Horizontales Rudern mit Dipstation und erhöhten Beinen", Description = "", ExternalRef1 = "619", IsBodyWeight = true, IsCompoundExercise = false  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Horizontales Rudern am Schlingentrainer", Description = "", ExternalRef1 = "620", IsBodyWeight = false, IsCompoundExercise = false  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Horizontales Rudern am Schlingentrainer mit erhöhten Beinen", Description = "", ExternalRef1 = "620", IsBodyWeight = false, IsCompoundExercise = false  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "ẞH Überzüge mit gestreckten Armen", Description = "", ExternalRef1 = "624", IsBodyWeight = false, IsCompoundExercise = false  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Überzüge mit gestreckten Armen", Description = "", ExternalRef1 = "624", IsBodyWeight = false, IsCompoundExercise = false  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Ziehen mit gestrecktem Arm", Description = "", ExternalRef1 = "627", IsBodyWeight = false, IsCompoundExercise = false  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Kreuzheben mit gestreckten Beinen", Description = "", ExternalRef1 = "629", IsBodyWeight = false, IsCompoundExercise = true  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Kreuzheben mit gestreckten Beinen", Description = "", ExternalRef1 = "629", IsBodyWeight = false, IsCompoundExercise = true  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Kreuzheben", Description = "", ExternalRef1 = "844", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Kreuzheben", Description = "", ExternalRef1 = "857", IsBodyWeight = false, IsCompoundExercise = false });

            // Trapez
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Schulterheben vor dem Körper", Description = "", ExternalRef1 = "634", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Schulterheben hinter dem Körper", Description = "", ExternalRef1 = "635", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Schulterheben im Stehen", Description = "", ExternalRef1 = "641", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Schulterheben im Sitzen", Description = "", ExternalRef1 = "642", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Schulterheben im Stehen einarmig", Description = "", ExternalRef1 = "643", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Schulterheben im Sitzen Bauchlage", Description = "45 Grad", ExternalRef1 = "644", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Dips mit gestreckten Armen", Description = "", ExternalRef1 = "650", IsBodyWeight = true, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Y-Heben", Description = "", ExternalRef1 = "653", IsBodyWeight = false, IsCompoundExercise = false });

            // Trizeps
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Bankdrücken mit engem Griff", Description = "", ExternalRef1 = "658", IsBodyWeight = false, IsCompoundExercise = true });
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Bankdrücken mit engem Untergriff", Description = "", ExternalRef1 = "659", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Bankdrücken mit engem Griff", Description = "", ExternalRef1 = "660", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Trizepsdips", Description = "Dipstation", ExternalRef1 = "661", IsBodyWeight = true, IsCompoundExercise = false  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Dips an der Bank", Description = "", ExternalRef1 = "662", IsBodyWeight = true, IsCompoundExercise = false  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Liegestütz mit engem Griff", Description = "", ExternalRef1 = "664", IsBodyWeight = false, IsCompoundExercise = true });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Trizepsdrücken mit Band", Description = "", ExternalRef1 = "671", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Kickbacks", Description = "", ExternalRef1 = "672", IsBodyWeight = false, IsCompoundExercise = false  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "ẞH Trizepsstrecken im Liegen", Description = "", ExternalRef1 = "675", IsBodyWeight = false, IsCompoundExercise = false  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Trizepsstrecken im Liegen", Description = "1 KH pro Arm", ExternalRef1 = "677", IsBodyWeight = false, IsCompoundExercise = false  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Trizepsstrecken im Liegen einarmig", Description = "", ExternalRef1 = "678", IsBodyWeight = false, IsCompoundExercise = false  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "ẞH Trizepsstrecken über Kopf", Description = "", ExternalRef1 = "680", IsBodyWeight = false, IsCompoundExercise = false  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Trizepsstrecken über Kopf", Description = "1 KH pro Arm", ExternalRef1 = "680", IsBodyWeight = false, IsCompoundExercise = false  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Trizepsstrecken über Kopf einarmig", Description = "", ExternalRef1 = "681", IsBodyWeight = false, IsCompoundExercise = false  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "ẞH Trizepsstrecken über Kopf in Rückenlage", Description = "45 Grad", ExternalRef1 = "680", IsBodyWeight = false, IsCompoundExercise = false  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Trizepsstrecken über Kopf in Rückenlage", Description = "1 KH pro Arm. 45 Grad", ExternalRef1 = "680", IsBodyWeight = false, IsCompoundExercise = false  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Trizepsstrecken über Kopf einarmig in Rückenlage", Description = "45 Grad", ExternalRef1 = "681", IsBodyWeight = false, IsCompoundExercise = false  });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Trizepsstrecken mit Schlingentrainer", Description = "", ExternalRef1 = "", IsBodyWeight = true, IsCompoundExercise = false  });

            // Bizeps
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Curls im Stehen im Untergriff", Description = "", ExternalRef1 = "690", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "ẞH Curls im Stehen im Untergriff", Description = "", ExternalRef1 = "690", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Curls im Stehen im Obergriff", Description = "", ExternalRef1 = "721", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "ẞH Curls im Stehen im Obergriff", Description = "", ExternalRef1 = "721", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Curls im Stehen", Description = "", ExternalRef1 = "691", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Curls im Stehen alternierend", Description = "", ExternalRef1 = "692", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Curls im Sitzen", Description = "90 Grad", ExternalRef1 = "696", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Curls im Sitzen alternierend", Description = "90 Grad", ExternalRef1 = "697", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Curls auf der Schrägbank Rückenlage", Description = "45 Grad", ExternalRef1 = "698", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Curls auf der Schrägbank Rückenlage alternierend", Description = "45 Grad", ExternalRef1 = "698", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Curls auf der Schrägbank Bauchlage", Description = "45 Grad", ExternalRef1 = "698", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Curls auf der Schrägbank Bauchlage alternierend", Description = "45 Grad", ExternalRef1 = "698", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Preachercurls", Description = "Scottcurls, Arm auf Schrägbank abgelegt", ExternalRef1 = "713", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Konzentrationscurls", Description = "", ExternalRef1 = "714", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Curls mit dem Schlingentrainer", Description = "", ExternalRef1 = "716", IsBodyWeight = true, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Curls im Stehen im Hammergriff", Description = "", ExternalRef1 = "717", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Curls im Stehen im Hammergriff alternierend", Description = "", ExternalRef1 = "718", IsBodyWeight = false, IsCompoundExercise = false });

            // Unterarme
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Unterarmcurls", Description = "", ExternalRef1 = "725", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Unterarmcurls", Description = "", ExternalRef1 = "726", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Unterarmcurls hinter dem Rücken im Stehen", Description = "727", ExternalRef1 = "", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Unterarmstrecken", Description = "", ExternalRef1 = "728", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Unterarmstrecken", Description = "", ExternalRef1 = "729", IsBodyWeight = false, IsCompoundExercise = false });

            // Quadrizeps
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Kniebeuge", Description = "", ExternalRef1 = "738", IsBodyWeight = false, IsCompoundExercise = true });
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Sumo-Kniebeuge", Description = "", ExternalRef1 = "", IsBodyWeight = false, IsCompoundExercise = true });
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Frontkniebeuge", Description = "", ExternalRef1 = "739", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Boxkniebeuge", Description = "", ExternalRef1 = "740", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Kniebeuge", Description = "", ExternalRef1 = "743", IsBodyWeight = false, IsCompoundExercise = true });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Frontkniebeuge", Description = "", ExternalRef1 = "744", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Hackenschmidt-Kniebeuge", Description = "", ExternalRef1 = "747", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Ausfallschritte", Description = "", ExternalRef1 = "756", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Ausfallschritte gehalten", Description = "", ExternalRef1 = "", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "ẞH Ausfallschritte gehalten", Description = "", ExternalRef1 = "", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Miniband Beinstrecken", Description = "", ExternalRef1 = "763", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Kniebeuge einbeinig", Description = "", ExternalRef1 = "", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "ẞH Kniebeuge einbeinig", Description = "", ExternalRef1 = "745", IsBodyWeight = false, IsCompoundExercise = false });

            // Beinbeuger
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Rumänisches Kreuzheben", Description = "", ExternalRef1 = "767", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Rumänisches Kreuzheben", Description = "", ExternalRef1 = "768", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Miniband Beincurls im Liegen", Description = "", ExternalRef1 = "773", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Beincurls", Description = "", ExternalRef1 = "774", IsBodyWeight = false, IsCompoundExercise = false });

            // Waden
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Wadenheben im Stehen", Description = "", ExternalRef1 = "", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Wadenheben einbeinig im Stehen", Description = "", ExternalRef1 = "", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Wadenheben einbeinig im Stehen mit Erhöhung", Description = "", ExternalRef1 = "", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Wadenheben im Sitzen", Description = "", ExternalRef1 = "", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Wadenheben im Sitzen", Description = "", ExternalRef1 = "", IsBodyWeight = false, IsCompoundExercise = false });

            // Bauch
            DatabaseContext.Exercises.Add(new Exercise { Name = "Crunches", Description = "", ExternalRef1 = "798", IsBodyWeight = false, IsCompoundExercise = true });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Crunches mit Radfahren", Description = "", ExternalRef1 = "805", IsBodyWeight = true, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Beinheben sitzend", Description = "In & Outs", ExternalRef1 = "810", IsBodyWeight = true, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "KH Klappmesser", Description = "", ExternalRef1 = "811", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Reverse Crunches", Description = "", ExternalRef1 = "812", IsBodyWeight = true, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Beckenheben", Description = "", ExternalRef1 = "815", IsBodyWeight = true, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Hängendes Knieheben", Description = "", ExternalRef1 = "817", IsBodyWeight = true, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Hängendes Beinheben", Description = "", ExternalRef1 = "818", IsBodyWeight = true, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Hängende Crunches am Schlingentrainer", Description = "", ExternalRef1 = "820", IsBodyWeight = true, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Diagonale Crunches", Description = "", ExternalRef1 = "822", IsBodyWeight = true, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Seitliche Crunches", Description = "", ExternalRef1 = "824", IsBodyWeight = true, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Seitliches Klappmesser", Description = "", ExternalRef1 = "828", IsBodyWeight = true, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Beinheben im Liegen", Description = "", ExternalRef1 = "832", IsBodyWeight = true, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Beinscheren im Liegen", Description = "", ExternalRef1 = "833", IsBodyWeight = true, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "LH Aufrollen", Description = "", ExternalRef1 = "835", IsBodyWeight = false, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Unterarmstütz", Description = "", ExternalRef1 = "838", IsBodyWeight = true, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Seitstütz", Description = "", ExternalRef1 = "839", IsBodyWeight = true, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Seitstütz mit Durchgreifen", Description = "", ExternalRef1 = "840", IsBodyWeight = true, IsCompoundExercise = false });
            DatabaseContext.Exercises.Add(new Exercise { Name = "Russian Twist", Description = "", ExternalRef1 = "", IsBodyWeight = true, IsCompoundExercise = false });

            DatabaseContext.SaveChanges();

            System.Console.WriteLine("Success");
        }
    }
}
