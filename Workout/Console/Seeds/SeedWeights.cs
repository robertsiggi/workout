﻿using Model;

namespace Console.Seeds
{
    internal partial class Seeds
    {
        internal void Weights()
        {
            System.Console.WriteLine("Weight seeds");

            // Plates
            DatabaseContext.Weights.Add(new Weight { Name = "S 01,25" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 02,5" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 03,75" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 05" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 06,25" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 07,5" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 08,75" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 10" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 11,25" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 12,5" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 13,75" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 15" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 16,25" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 17,5" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 18,75" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 20" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 21,25" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 22,5" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 23,75" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 25" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 26,25" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 27,5" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 28,75" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 30" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 31,25" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 32,5" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 33,75" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 35" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 36,25" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 37,5" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 38,75" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 40" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 41,25" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 42,5" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 43,75" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 45" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 46,25" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 47,5" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 48,75" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 50" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 51,25" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 52,5" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 53,75" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 55" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 56,25" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 57,5" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 58,75" });
            DatabaseContext.Weights.Add(new Weight { Name = "S 60" });

            // Kettlebells
            DatabaseContext.Weights.Add(new Weight { Name = "KB 04" });
            DatabaseContext.Weights.Add(new Weight { Name = "KB 08" });
            DatabaseContext.Weights.Add(new Weight { Name = "KB 12" });

            // Widerstandsbänder
            DatabaseContext.Weights.Add(new Weight { Name = "MB 01 Leight" });
            DatabaseContext.Weights.Add(new Weight { Name = "MB 02 Medium" });
            DatabaseContext.Weights.Add(new Weight { Name = "MB 03 Heavy" });
            DatabaseContext.Weights.Add(new Weight { Name = "MB 04 X-Heavy" });
            DatabaseContext.Weights.Add(new Weight { Name = "MB 05 X-Heavy + Leight" });
            DatabaseContext.Weights.Add(new Weight { Name = "MB 06 X-Heavy + Medium" });
            DatabaseContext.Weights.Add(new Weight { Name = "MB 07 X-Heavy + Heavy" });
            DatabaseContext.Weights.Add(new Weight { Name = "MB 08 X-Heavy + Heavy + Leight" });
            DatabaseContext.Weights.Add(new Weight { Name = "MB 09 X-Heavy + Heavy + Medium" });
            DatabaseContext.Weights.Add(new Weight { Name = "MB 10 X-Heavy + Heavy + Medium + Leight" });

            DatabaseContext.SaveChanges();

            System.Console.WriteLine("Success");
        }
    }
}
