﻿using Model;

namespace Console.Seeds
{
    internal partial class Seeds
    {
        internal void Muscles()
        {
            System.Console.WriteLine("Muscle seeds");

            //var repoMuscles = unitOfWork.MuscleRepository;

            //// Brust
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Brust", MuscleGroupLatin = "M. pectoralis major", MuscleGroupOther = "", MuscleGerman = "obere Brust", MuscleLatin = "", MuscleOther = "upper Chest", MinSetsPerWeek = 0 });
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Brust", MuscleGroupLatin = "M. pectoralis major", MuscleGroupOther = "", MuscleGerman = "Brust", MuscleLatin = "", MuscleOther = "Chest", MinSetsPerWeek = 0 });
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Brust", MuscleGroupLatin = "M. pectoralis major", MuscleGroupOther = "", MuscleGerman = "untere Brust", MuscleLatin = "", MuscleOther = "lower Chest", MinSetsPerWeek = 0 });

            //// Schultern
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Schultern", MuscleGroupLatin = "M. deltoideus", MuscleGroupOther = "", MuscleGerman = "vordere Schulter", MuscleLatin = "", MuscleOther = "", MinSetsPerWeek = 0 });
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Schultern", MuscleGroupLatin = "M. deltoideus", MuscleGroupOther = "", MuscleGerman = "mittlere Schulter", MuscleLatin = "", MuscleOther = "Shoulders, Delts", MinSetsPerWeek = 0 });
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Schultern", MuscleGroupLatin = "M. deltoideus", MuscleGroupOther = "", MuscleGerman = "hintere Schulter", MuscleLatin = "", MuscleOther = "", MinSetsPerWeek = 0 });

            //// Rücken
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Rücken", MuscleGroupLatin = "", MuscleGroupOther = "", MuscleGerman = "breiter Rückenmuskel", MuscleLatin = "M. latissimus dorsi", MuscleOther = "Lats", MinSetsPerWeek = 0 });
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Rücken", MuscleGroupLatin = "", MuscleGroupOther = "", MuscleGerman = "Rautenmuskel", MuscleLatin = "M. rhomboideus", MuscleOther = "Middle back, Rhomboids", MinSetsPerWeek = 0 });
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Rücken", MuscleGroupLatin = "", MuscleGroupOther = "", MuscleGerman = "großer Rundmuskel", MuscleLatin = "M. teres major", MuscleOther = "", MinSetsPerWeek = 0 });
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Rücken", MuscleGroupLatin = "", MuscleGroupOther = "", MuscleGerman = "unterer Rückenstrecker", MuscleLatin = "M. erector spinae", MuscleOther = "", MinSetsPerWeek = 0 });

            //// Trapez
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Trapez", MuscleGroupLatin = "M. trapezius", MuscleGroupOther = "", MuscleGerman = "oberer Trapez", MuscleLatin = "", MuscleOther = "Traps", MinSetsPerWeek = 0 });
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Trapez", MuscleGroupLatin = "M. trapezius", MuscleGroupOther = "", MuscleGerman = "mittlerer Trapez", MuscleLatin = "", MuscleOther = "Traps", MinSetsPerWeek = 0 });
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Trapez", MuscleGroupLatin = "M. trapezius", MuscleGroupOther = "", MuscleGerman = "unterer Trapez", MuscleLatin = "", MuscleOther = "Traps", MinSetsPerWeek = 0 });

            //// Trizeps
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Trizeps", MuscleGroupLatin = "M. triceps brachii", MuscleGroupOther = "", MuscleGerman = "seitlicher Trizeps-Kopf", MuscleLatin = "", MuscleOther = "", MinSetsPerWeek = 0 });
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Trizeps", MuscleGroupLatin = "M. triceps brachii", MuscleGroupOther = "", MuscleGerman = "langer Trizeps-Kopf", MuscleLatin = "", MuscleOther = "", MinSetsPerWeek = 0 });
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Trizeps", MuscleGroupLatin = "M. triceps brachii", MuscleGroupOther = "", MuscleGerman = "mittlerer Trizeps-Kopf", MuscleLatin = "", MuscleOther = "", MinSetsPerWeek = 0 });

            //// Bizeps
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Bizeps", MuscleGroupLatin = "M. biceps brachii", MuscleGroupOther = "", MuscleGerman = "langer Bizeps-Kopf", MuscleLatin = "", MuscleOther = "", MinSetsPerWeek = 0 });
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Bizeps", MuscleGroupLatin = "M. biceps brachii", MuscleGroupOther = "", MuscleGerman = "kurzer Bizeps-Kopf", MuscleLatin = "", MuscleOther = "", MinSetsPerWeek = 0 });
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Bizeps", MuscleGroupLatin = "M. biceps brachii", MuscleGroupOther = "", MuscleGerman = "Armbeuger", MuscleLatin = "M. brachialis", MuscleOther = "", MinSetsPerWeek = 0 });

            //// Unterarme
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Unterarme", MuscleGroupLatin = "", MuscleGroupOther = "", MuscleGerman = "Unterarmbeuger", MuscleLatin = "", MuscleOther = "Flexoren, Forearm", MinSetsPerWeek = 0 });
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Unterarme", MuscleGroupLatin = "", MuscleGroupOther = "", MuscleGerman = "Unterarmstrecker", MuscleLatin = "", MuscleOther = "Extensoren, Forearm", MinSetsPerWeek = 0 });

            //// Quadrizeps
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Quadrizeps", MuscleGroupLatin = "M. quadriceps femoris", MuscleGroupOther = "Oberschenkelmuskel", MuscleGerman = "äußerer Oberschenkelmuskel", MuscleLatin = "M. vastus lateralis", MuscleOther = "Quads", MinSetsPerWeek = 0 });
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Quadrizeps", MuscleGroupLatin = "M. quadriceps femoris", MuscleGroupOther = "Oberschenkelmuskel", MuscleGerman = "innerer Oberschenkelmuskel", MuscleLatin = "M. vastus medialis", MuscleOther = "Quads", MinSetsPerWeek = 0 });
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Quadrizeps", MuscleGroupLatin = "M. quadriceps femoris", MuscleGroupOther = "Oberschenkelmuskel", MuscleGerman = "mittlerer Oberschenkelmuskel", MuscleLatin = "M. vastus intermedius", MuscleOther = "Quads", MinSetsPerWeek = 0 });
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Quadrizeps", MuscleGroupLatin = "M. quadriceps femoris", MuscleGroupOther = "Oberschenkelmuskel", MuscleGerman = "gerader Oberschenkelmuskel", MuscleLatin = "M. rectus femoris", MuscleOther = "Quads", MinSetsPerWeek = 0 });

            //// Hinterer Oberschenkel
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Hinterer Oberschenkel", MuscleGroupLatin = "", MuscleGroupOther = "", MuscleGerman = "Beinbeuger", MuscleLatin = "M. biceps femoris", MuscleOther = "Hamstrings", MinSetsPerWeek = 0 });
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Hinterer Oberschenkel", MuscleGroupLatin = "", MuscleGroupOther = "", MuscleGerman = "", MuscleLatin = "M. semitendinosus", MuscleOther = "Hamstrings", MinSetsPerWeek = 0 });
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Hinterer Oberschenkel", MuscleGroupLatin = "", MuscleGroupOther = "", MuscleGerman = "", MuscleLatin = "M. semimembranosus", MuscleOther = "Hamstrings", MinSetsPerWeek = 0 });

            //// Gesäß
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Gesäß", MuscleGroupLatin = "", MuscleGroupOther = "", MuscleGerman = "großer Gesäßmuskel", MuscleLatin = "M. gluteus maximus", MuscleOther = "Glutes", MinSetsPerWeek = 0 });
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Gesäß", MuscleGroupLatin = "", MuscleGroupOther = "", MuscleGerman = "mittlerer Gesäßmuskel", MuscleLatin = "M. gluteus medius", MuscleOther = "Glutes", MinSetsPerWeek = 0 });

            //// Waden
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Waden", MuscleGroupLatin = "", MuscleGroupOther = "", MuscleGerman = "Zwillingswadenmuskel", MuscleLatin = "M. gastrocnemius", MuscleOther = "Calves", MinSetsPerWeek = 0 });
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Waden", MuscleGroupLatin = "", MuscleGroupOther = "", MuscleGerman = "Schollenmuskel", MuscleLatin = "M. soleus", MuscleOther = "", MinSetsPerWeek = 0 });
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Waden", MuscleGroupLatin = "", MuscleGroupOther = "", MuscleGerman = "vorderer Schienbeinmuskel", MuscleLatin = "M. tibialis anterior", MuscleOther = "", MinSetsPerWeek = 0 });

            //// Bauch und Körpermitte
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Bauch und Körpermitte", MuscleGroupLatin = "", MuscleGroupOther = "", MuscleGerman = "gerader Bauchmuskel", MuscleLatin = "M. rectus abdominis", MuscleOther = "", MinSetsPerWeek = 0 });
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Bauch und Körpermitte", MuscleGroupLatin = "", MuscleGroupOther = "", MuscleGerman = "äußerer schräger Bauchmuskel", MuscleLatin = "M. obliquus externus abdominis", MuscleOther = "", MinSetsPerWeek = 0 });
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Bauch und Körpermitte", MuscleGroupLatin = "", MuscleGroupOther = "", MuscleGerman = "innerer schräger Bauchmuskel", MuscleLatin = "M. obliquus internus abdominis", MuscleOther = "", MinSetsPerWeek = 0 });
            //repoMuscles.Add(new Muscle { MuscleGroupGerman = "Bauch und Körpermitte", MuscleGroupLatin = "", MuscleGroupOther = "", MuscleGerman = "querer Bauchmuskel", MuscleLatin = "M. transversus abdominis", MuscleOther = "", MinSetsPerWeek = 0 });

            //unitOfWork.SaveChanges();

            System.Console.WriteLine("Success");
        }
    }
}
