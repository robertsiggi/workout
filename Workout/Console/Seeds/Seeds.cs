﻿using Database;

namespace Console.Seeds
{
    internal partial class Seeds
    {
        private readonly IDatabaseContext DatabaseContext;

        public Seeds(IDatabaseContext databaseContext)
        {
            DatabaseContext = databaseContext;
        }
    }
}
