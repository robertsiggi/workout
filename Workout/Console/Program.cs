﻿using console;
using Database;
using Microsoft.Extensions.Configuration;
using System;

namespace Console
{
    internal static class Program
    {
        private static AppSettings appConfig;
        private static void Main(string[] args)
        {
            LoadConfig();

            System.Console.WriteLine("0 - Create/Migrate Database");
            System.Console.WriteLine("1 - Seeds");
            System.Console.WriteLine("2 - Testfunktion");
            System.Console.WriteLine(string.Empty);

            System.Console.WriteLine("Your input:");
            var key = System.Console.ReadKey().Key;
            System.Console.WriteLine(string.Empty);

            try
            {
                switch (key)
                {
                    case ConsoleKey.D0:
                        CreateAndMigrateDatabase();
                        break;
                    case ConsoleKey.D1:
                        SeedDatabase();
                        break;
                    case ConsoleKey.D2:
                        Test();
                        break;
                    default:
                        System.Console.WriteLine("Invalid action");
                        break;
                }
            }
            catch (Exception e)
            {
                System.Console.WriteLine("Exception:");
                System.Console.WriteLine(e);
            }
        }

        private static void LoadConfig()
        {
            var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").AddJsonFile("appsettings.Development.json").Build();
            appConfig = config.GetSection("App").Get<AppSettings>();
        }

        private static void CreateAndMigrateDatabase()
        {
            System.Console.WriteLine("Creating database...");
            using (var dbContext = DatabaseContextFactory.CreateAndMigrateDatabase(appConfig.ConnectionString))
            {
                System.Console.WriteLine("Database created!");
            }
        }

        private static void SeedDatabase()
        {
            System.Console.WriteLine("Seeding database...");
            using (var databaseContext = DatabaseContextFactory.CreateDatabaseContext(appConfig.ConnectionString))
            {
                var seeds = new Seeds.Seeds(databaseContext);
                seeds.Pause();
                seeds.Muscles();
                seeds.Weights();
                seeds.Intervals();
                seeds.Exercises();
            }
        }

        private static void Test()
        {
            System.Console.WriteLine("Test function started");
            //using (var unitOfWork = new UnitOfWork<IDatabaseContext>(DatabaseContextFactory.CreateDatabaseContext(appConfig.ConnectionString)))
            //{
                //var repoReceipts = unitOfWork.ReceiptRepository;
                //var receipt = repoReceipts.GetByID(new Guid("07836445-FCEB-4E6D-3E42-08D7A60E4EDA"));
                //System.Console.WriteLine(receipt.CreatedDate);
            //}
        }
    }
}
