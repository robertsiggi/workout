﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Model;
using System;
using System.Diagnostics.CodeAnalysis;

namespace Database
{
    public interface IDatabaseContext: IDisposable
    {
        public ChangeTracker ChangeTracker { get; }
        public DatabaseFacade Database { get; }
        public IModel Model { get; }
        public DbContextId ContextId { get; }
        public EntityEntry Entry([NotNullAttribute] object entity);
        public EntityEntry<TEntity> Entry<TEntity>([NotNullAttribute] TEntity entity) where TEntity : class;

        //DbSet<TEntity> Set<TEntity>() where TEntity : class;

        DbSet<Exercise> Exercises { get; }
        DbSet<Exercise2ExerciseSelection> Exercise2ExerciseSelections { get; }
        DbSet<ExerciseSelection> ExerciseSelections { get; }
        DbSet<Interval> Intervals { get; }
        DbSet<Muscle> Muscles { get; }
        DbSet<Muscle2MuscleGroup> Muscle2MuscleGroups { get; }
        DbSet<MuscleGroup> MuscleGroups { get; }
        DbSet<Pause> Pauses { get; }
        DbSet<Progress> Progresses { get; }
        DbSet<Training> Trainings { get; }
        DbSet<TrainingExercise> TrainingExercises { get; }
        DbSet<TrainingSet> TrainingSets { get; }
        DbSet<UserMeasurement> UserMeasurements { get; }
        DbSet<Weight> Weights { get; }

        int SaveChanges();
    }
}
