﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Database
{
    public class DatabaseContextFactory : IDesignTimeDbContextFactory<DatabaseContext>
    {
        public static string ConnectionString { get; private set; }
        public DatabaseContext CreateDbContext(string connectionString)
        {
            return CreateDatabaseContext(connectionString);
        }

        public static DatabaseContext CreateAndMigrateDatabase(string connectionString)
        {
            return CreateDatabaseContext(connectionString, true);
        }

        public static DatabaseContext CreateDatabaseContext(string connectionString)
        {
            return CreateDatabaseContext(connectionString, false);
        }

        private static DatabaseContext CreateDatabaseContext(string connectionString, bool createIfNotExists)
        {
            ConnectionString = connectionString;

            DbContextOptionsBuilder<DatabaseContext> builder = new DbContextOptionsBuilder<DatabaseContext>();
            // builder.UseSqlServer(connectionString);
            builder.UseSqlite(connectionString);

            return new DatabaseContext(builder.Options, createIfNotExists);
        }

        private static DatabaseContext CreateDatabaseContext(DbContextOptionsBuilder<DatabaseContext> builder)
        {
            return new DatabaseContext(builder.Options);
        }

        // ACCESSED BY MIGRATION
        public DatabaseContext CreateDbContext(string[] args)
        {
            // SQL EXPRESS:
            //const string connectionString = "Server=CN00063\\SQLEXPRESS;Database=Workout;Trusted_Connection=True;MultipleActiveResultSets=true";
            //return CreateDatabaseContext(connectionString);

            // PROD:
            const string databaseFilePath = "Data Source=workout.db";
            DbContextOptionsBuilder<DatabaseContext> builder = new DbContextOptionsBuilder<DatabaseContext>();
            builder.UseSqlite(databaseFilePath);

            return CreateDatabaseContext(builder);
        }
    }
}
