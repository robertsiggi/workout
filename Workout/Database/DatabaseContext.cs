﻿using Microsoft.EntityFrameworkCore;
using Model;
using Model.Base;
using System;
using System.Linq;

namespace Database
{
    public class DatabaseContext : DbContext, IDatabaseContext
    {
        public DbSet<Exercise> Exercises { get; private set; }
        public DbSet<Exercise2ExerciseSelection> Exercise2ExerciseSelections { get; private set;}
        public DbSet<ExerciseSelection> ExerciseSelections { get; private set; }
        public DbSet<Interval> Intervals { get; private set; }
        public DbSet<Muscle> Muscles { get; private set; }
        public DbSet<Muscle2MuscleGroup> Muscle2MuscleGroups { get; private set; }
        public DbSet<MuscleGroup> MuscleGroups { get; private set; }
        public DbSet<Pause> Pauses { get; private set; }
        public DbSet<Progress> Progresses { get; private set; }
        public DbSet<Training> Trainings { get; private set; }
        public DbSet<TrainingExercise> TrainingExercises { get; private set; }
        public DbSet<TrainingSet> TrainingSets { get; private set; }
        public DbSet<UserMeasurement> UserMeasurements { get; private set; }
        public DbSet<Weight> Weights { get; private set; }

        public DatabaseContext(DbContextOptions options, bool createIfNotExists = false) : base(options)
        {
            if (createIfNotExists)
            {
                if (Database.CanConnect())
                {
                    System.Console.WriteLine("Database connection ok");

                    System.Console.WriteLine("Applied Migrations:");
                    var appliedMigrations = Database.GetAppliedMigrations();
                    foreach (var appliedMigration in appliedMigrations)
                    {
                        System.Console.WriteLine(appliedMigration);
                    }

                    System.Console.WriteLine("Pending Migrations:");
                    var pendingMigrations = Database.GetPendingMigrations();
                    if (pendingMigrations.Any())
                    {
                        foreach (var pendingMigration in pendingMigrations)
                        {
                            System.Console.WriteLine(pendingMigration);
                        }
                    }
                    else
                    {
                        System.Console.WriteLine("No migration required");
                    }
                }
                else
                {
                    System.Console.WriteLine("No database connection - Creating new database");
                }

                Database.Migrate();
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var modelCreating = new ModelCreating.ModelCreating(modelBuilder);
            modelCreating.SetRelations();
        }

        public override int SaveChanges()
        {
            UpdateAuditEntities();
            return base.SaveChanges();
        }
        
        public void UpdateAuditEntities()
        {
            foreach (var entry in ChangeTracker.Entries().Where(x => x.Entity is IBaseModel && (x.State == EntityState.Added || x.State == EntityState.Modified)))
            {
                var entity = (IBaseModel)entry.Entity;

                DateTime now = DateTime.UtcNow;

                if (entry.State == EntityState.Added)
                {
                    entity.CreatedDate = now;
                }
                else
                {
                    base.Entry(entity).Property(x => x.CreatedDate).IsModified = false;
                }

                entity.EditedDate = now;
            }
        }
    }
}
