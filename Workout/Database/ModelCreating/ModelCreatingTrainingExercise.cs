﻿using Microsoft.EntityFrameworkCore;
using Model;

namespace Database.ModelCreating
{
    public partial class ModelCreating
    {
        private void TrainingExercise()
        {
            modelBuilder.Entity<TrainingExercise>().ToTable("TrainingTrainingExercises");

            modelBuilder.Entity<TrainingExercise>()
               .HasOne(x => x.TrainingSet)
               .WithMany(y => y.TrainingExercises)
               .HasForeignKey(x => x.TrainingSetId);

            modelBuilder.Entity<TrainingExercise>()
               .HasOne(x => x.Exercise)
               .WithMany(y => y.TrainingExercises)
               .HasForeignKey(x => x.ExerciseId);

            modelBuilder.Entity<TrainingExercise>()
               .HasOne(x => x.Interval)
               .WithMany(y => y.TrainingExercises)
               .HasForeignKey(x => x.IntervalId);

            modelBuilder.Entity<TrainingExercise>()
               .HasOne(x => x.Weight)
               .WithMany(y => y.TrainingExercises)
               .HasForeignKey(x => x.WeightId);
        }
    }
}
