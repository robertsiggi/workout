﻿using Microsoft.EntityFrameworkCore;
using Model;

namespace Database.ModelCreating
{
    public partial class ModelCreating
    {
        private void ExerciseSelection()
        {
            modelBuilder.Entity<ExerciseSelection>().ToTable("ExerciseSelections");
        }
    }
}
