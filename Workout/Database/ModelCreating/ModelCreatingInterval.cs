﻿using Microsoft.EntityFrameworkCore;
using Model;

namespace Database.ModelCreating
{
    public partial class ModelCreating
    {
        private void Interval()
        {
            modelBuilder.Entity<Interval>().ToTable("Intervals");

            modelBuilder.Entity<Interval>()
               .HasOne(x => x.Pause)
               .WithMany(y => y.Intervals)
               .HasForeignKey(x => x.PauseId);
        }
    }
}
