﻿using Microsoft.EntityFrameworkCore;
using Model;

namespace Database.ModelCreating
{
    public partial class ModelCreating
    {
        private void Muscle2MuscleGoup()
        {
            modelBuilder.Entity<Muscle2MuscleGroup>().ToTable("Muscle2MuscleGroups");

            modelBuilder.Entity<Muscle2MuscleGroup>()
               .HasOne(x => x.Muscle)
               .WithMany()
               .HasForeignKey(x => x.MuscleId);

            modelBuilder.Entity<Muscle2MuscleGroup>()
               .HasOne(x => x.MuscleGroup)
               .WithMany()
               .HasForeignKey(x => x.MuscleGroupId);
        }
    }
}
