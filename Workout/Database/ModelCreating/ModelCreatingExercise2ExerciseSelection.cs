﻿using Microsoft.EntityFrameworkCore;
using Model;

namespace Database.ModelCreating
{
    public partial class ModelCreating
    {
        private void Exercise2ExerciseSelection()
        {
            modelBuilder.Entity<Exercise2ExerciseSelection>().ToTable("Exercise2ExerciseSelections");

            modelBuilder.Entity<Exercise2ExerciseSelection>()
               .HasOne(x => x.Exercise)
               .WithMany(y => y.ExerciseSelections)
               .HasForeignKey(x => x.ExerciseId);

            modelBuilder.Entity<Exercise2ExerciseSelection>()
               .HasOne(x => x.ExerciseSelection)
               .WithMany(y => y.Exercises)
               .HasForeignKey(x => x.ExerciseSelectionId);
        }
    }
}
