﻿using Microsoft.EntityFrameworkCore;
using Model;

namespace Database.ModelCreating
{
    public partial class ModelCreating
    {
        private void Exercise()
        {
            modelBuilder.Entity<Exercise>().ToTable("Exercises");

            modelBuilder.Entity<Exercise>()
               .HasOne(x => x.Muscle1)
               .WithMany()
               .HasForeignKey(x => x.Muscle1Id);

            modelBuilder.Entity<Exercise>()
               .HasOne(x => x.Muscle2)
               .WithMany()
               .HasForeignKey(x => x.Muscle2Id);
        }
    }
}
