﻿using Microsoft.EntityFrameworkCore;

namespace Database.ModelCreating
{
    public partial class ModelCreating
    {
        private readonly ModelBuilder modelBuilder;

        public ModelCreating(ModelBuilder modelBuilder)
        {
            this.modelBuilder = modelBuilder;
        }

        public void SetRelations()
        {
            Exercise();
            ExerciseSelection();
            Exercise2ExerciseSelection();
            Interval();
            Muscle();
            MuscleGroup();
            Muscle2MuscleGoup();
            Pause();
            Progress();
            Training();
            TrainingExercise();
            TrainingSet();
            UserMeasurement();
            Weight();
        }
    }
}
