﻿using Microsoft.EntityFrameworkCore;
using Model;

namespace Database.ModelCreating
{
    public partial class ModelCreating
    {
        private void Pause()
        {
            modelBuilder.Entity<Pause>().ToTable("Pause");
        }
    }
}
