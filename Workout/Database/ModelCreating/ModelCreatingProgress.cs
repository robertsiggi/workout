﻿using Microsoft.EntityFrameworkCore;
using Model;

namespace Database.ModelCreating
{
    public partial class ModelCreating
    {
        private void Progress()
        {
            modelBuilder.Entity<Progress>().ToTable("Progresses");

            modelBuilder.Entity<Progress>()
               .HasOne(x => x.Exercise)
               .WithMany(y => y.Progresses)
               .HasForeignKey(x => x.ExerciseId);

            modelBuilder.Entity<Progress>()
               .HasOne(x => x.Interval)
               .WithMany(y => y.Progresses)
               .HasForeignKey(x => x.IntervalId);

            modelBuilder.Entity<Progress>()
               .HasOne(x => x.Weight)
               .WithMany(y => y.Progresses)
               .HasForeignKey(x => x.WeightId);
        }
    }
}
