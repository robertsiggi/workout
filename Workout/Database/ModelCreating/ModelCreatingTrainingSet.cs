﻿using Microsoft.EntityFrameworkCore;
using Model;

namespace Database.ModelCreating
{
    public partial class ModelCreating
    {
        private void TrainingSet()
        {
            modelBuilder.Entity<TrainingSet>().ToTable("TrainingSets");

            modelBuilder.Entity<TrainingSet>()
               .HasOne(x => x.Training)
               .WithMany(y => y.TrainingSets)
               .HasForeignKey(x => x.TrainingId);
        }
    }
}
