﻿using Model.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Model
{
    public class Muscle : BaseModel
    {
        [StringLength(100)] 
        public string NameGerman { get; set; }
        [StringLength(100)] 
        public string NameLatin { get; set; }
        [StringLength(100)] 
        public string NameOther { get; set; }

        public virtual ICollection<Muscle2MuscleGroup> MuscleGroups { get; set; }
    }
}
