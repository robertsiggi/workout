﻿using Model.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Model
{
    public class TrainingSet: BaseModel
    {
        [StringLength(100)]
        public string Name { get; set; }

        public Guid TrainingId { get; set; }
        public Training Training { get; set; }

        public virtual ICollection<TrainingExercise> TrainingExercises { get; set; }
    }
}
