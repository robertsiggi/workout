﻿using Model.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Model
{
    public class Pause : BaseModel
    {
        [StringLength(100)]
        public string Value { get; set; }

        public virtual ICollection<Interval> Intervals { get; set; }
    }
}
