﻿using Model.Base;
using System;

namespace Model
{
    public class TrainingExercise: BaseModel
    {
        public Guid TrainingSetId { get; set; }
        public TrainingSet TrainingSet { get; set; }

        public Guid ExerciseId { get; set; }
        public Exercise Exercise { get; set; }

        public Guid WeightId { get; set; }
        public Weight Weight { get; set; }

        public Guid IntervalId { get; set; }
        public Interval Interval { get; set; }
    }
}
