﻿using Model.Base;
using System;
using System.Collections.Generic;

namespace Model
{
    public class Interval : BaseModel
    {
        public int Sets { get; set; }
        public int RepsMin { get; set; }
        public int RepsMax { get; set; }
        public int RepDuration { get; set; }
        public int ExerciseNo { get; set; }

        public Guid PauseId { get; set; }
        public Pause Pause { get; set; }

        public virtual ICollection<Progress> Progresses { get; set; }
        public virtual ICollection<TrainingExercise> TrainingExercises { get; set; }
    }
}
