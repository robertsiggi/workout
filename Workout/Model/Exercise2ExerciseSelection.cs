﻿using Model.Base;
using System;

namespace Model
{
    public class Exercise2ExerciseSelection: BaseModel
    {
        public Guid? ExerciseId { get; set; }
        public Exercise Exercise { get; set; }

        public Guid? ExerciseSelectionId { get; set; }
        public ExerciseSelection ExerciseSelection { get; set; }
    }
}
