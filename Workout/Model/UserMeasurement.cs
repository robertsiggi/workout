﻿using System;
using Model.Base;

namespace Model
{
    public class UserMeasurement: BaseModel
    {
        public DateTime Date { get; set; }
        public float Weight { get; set; }
        public float UpperArmLeft { get; set; }
        public float UpperArmRight { get; set; }
        public float ForeArmLeft { get; set; }
        public float ForeArmRight { get; set; }
        public float Chest { get; set; }
        public float ThighLeft { get; set; }
        public float ThighRight { get; set; }
        public float CalfLeft { get; set; }
        public float CalfRight { get; set; }
        public float Waist { get; set; }
        public float Shoulder { get; set; }
    }
}
