﻿using Model.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Model
{
    public class Weight : BaseModel
    {
        [StringLength(100)]
        public string Name { get; set; }

        public virtual ICollection<Progress> Progresses { get; set; }
        public virtual ICollection<TrainingExercise> TrainingExercises { get; set; }
    }
}
