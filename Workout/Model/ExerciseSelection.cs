﻿using Model.Base;
using System.Collections.Generic;

namespace Model
{
    public class ExerciseSelection: BaseModel
    {
        public string Name { get; set; }
        public int SetsPerWeek { get; set; }

        public virtual ICollection<Exercise2ExerciseSelection> Exercises { get; set; }
    }
}
