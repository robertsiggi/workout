﻿using Model.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Model
{
    public class MuscleGroup : BaseModel
    {
        [StringLength(100)] 
        public string NameGerman { get; set; }
        [StringLength(100)] 
        public string NameLatin { get; set; }
        [StringLength(100)] 
        public string NameOther { get; set; }

        public int MinSetsPerWeek { get; set; }
        public int MinExercisesPerWeek { get; set; }

        public virtual ICollection<Muscle2MuscleGroup> Muscles { get; set; }
    }
}
