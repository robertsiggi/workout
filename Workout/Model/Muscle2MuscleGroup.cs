﻿using Model.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Model
{
    public class Muscle2MuscleGroup : BaseModel
    {
        public Guid MuscleId { get; set; }
        public Muscle Muscle { get; set; }

        public Guid MuscleGroupId { get; set; }
        public MuscleGroup MuscleGroup { get; set; }
    }
}
