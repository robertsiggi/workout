﻿using Model.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Model
{
    public class Exercise : BaseModel
    {
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(10000)]
        public string Description { get; set; }

        public bool IsBodyWeight { get; set; }
        public bool IsCompoundExercise { get; set; }
        public bool IsActive { get; set; }

        [StringLength(2000)]
        public string ExternalRef1 { get; set; }
        [StringLength(2000)]
        public string ExternalRef2 { get; set; }

        public Guid? Muscle1Id { get; set; }
        public Muscle Muscle1 { get; set; }

        public Guid? Muscle2Id { get; set; }
        public Muscle Muscle2 { get; set; }


        public virtual ICollection<Progress> Progresses { get; set; }
        public virtual ICollection<TrainingExercise> TrainingExercises { get; set; }
        public virtual ICollection<Exercise2ExerciseSelection> ExerciseSelections { get; set; }
    }
}
