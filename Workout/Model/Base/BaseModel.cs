﻿using System;

namespace Model.Base
{
    public abstract class BaseModel : IBaseModel
    {
        public Guid Id { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? EditedDate { get; set; }
    }
}
