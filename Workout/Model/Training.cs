﻿using Model.Base;
using System;
using System.Collections.Generic;

namespace Model
{
    public class Training: BaseModel
    {
        public DateTime Day{ get; set; }

        public virtual ICollection<TrainingSet> TrainingSets { get; set; }
    }
}
