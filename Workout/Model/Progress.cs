﻿using Model.Base;
using System;

namespace Model
{
    public class Progress: BaseModel
    {
        public Guid ExerciseId { get; set; }
        public Exercise Exercise { get; set; }

        public Guid IntervalId { get; set; }
        public Interval Interval { get; set; }

        public Guid WeightId { get; set; }
        public Weight Weight { get; set; }
    }
}
